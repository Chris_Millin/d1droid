﻿using DiscordDomain;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace D1Droid
{
    /// <summary>
    /// Client which runs the script domains
    /// </summary>
    public class D1DiscordClient : MarshalByRefObject, IDiscordDomainHandlerProxy
    {
        /// <summary>
        /// Holder for the Domain and script manager
        /// </summary>
        private class D1Domain
        {
            public DiscordScriptDomain ScriptDomain;
            public AppDomain Domain;
        }

        private D1Domain m_mainDomain = null;
        private D1Domain m_secondaryDomain = null;
        private CommandLineArgs m_commandLineArgs;

        private DateTime m_appStart = DateTime.Now;

        private bool m_unloadingDomain = false;
        
        /// <summary>
        /// Start the client
        /// </summary>
        /// <param name="arguments"></param>
        public D1DiscordClient(CommandLineArgs arguments)
        {
            m_commandLineArgs = arguments;
        }

        public bool StartClient()
        {
            string name = $"DiscordDomain_{Guid.NewGuid().ToString()}";
            
            try
            {
                StartDomain(name, ref m_mainDomain);
                SendProxyMessage(m_mainDomain.ScriptDomain, new DiscordInitaliseDomainEvent(m_commandLineArgs, m_appStart));
                SendProxyMessage(m_mainDomain.ScriptDomain, new DiscordDomainEvent(ProxyMessageType.RequestLoadAllServers));
                return true;
            }
            catch (Exception ex)
            {
                Logging.LogException(LogType.Client, ex, $"Failed to build domain {name}");

                m_mainDomain.ScriptDomain.Dispose();
                UnloadDomain(m_mainDomain.Domain);

                m_mainDomain.Domain         = null;
                m_mainDomain.ScriptDomain   = null;
                m_mainDomain                = null;

                return false;
            }
        }

        public bool StopClient()
        {
            return true;
        }

        private void StartDomain(string name, ref D1Domain domain)
        {
            if (domain == null)
                domain = new D1Domain();

            Logging.LogInfo(LogType.Client, $"Starting Domain {name}");

            AppDomain appDomain = AppDomain.CreateDomain(name, null);

            string assemblyName     = typeof(DiscordScriptDomain).Assembly.FullName;
            string typeName         = typeof(DiscordScriptDomain).FullName;
            object scriptDomain     = appDomain.CreateInstanceAndUnwrap(assemblyName, typeName, false, BindingFlags.Instance | BindingFlags.Public, null, new[] { this }, null, null);
            DiscordScriptDomain discordScriptDomain = (DiscordScriptDomain)scriptDomain;

            domain.Domain       = appDomain;
            domain.ScriptDomain = discordScriptDomain;

            Logging.LogInfo(LogType.Client, $"Domain {name} has been created");
        }

        public void ProxyMessage(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy msg)
        {
            switch(msg.ProxyMessage)
            {
                case ProxyMessageType.RequestUnloadDomain:
                    ShuttingDownDomainEvent(sender, msg);
                    break;
                case ProxyMessageType.UnloadedServer:
                    System.Threading.ThreadPool.QueueUserWorkItem((object param) => UnloadServerEvent(sender, msg));
                    break;
                case ProxyMessageType.UnloadDomain:
                    UnloadDomain(sender, msg);
                    break;
            }
        }

        public void SendProxyMessage(IDiscordDomainHandlerProxy reciever, IDiscordDomainEventProxy message)
        {
            reciever.ProxyMessage(this, message);
        }

        private void ShuttingDownDomainEvent(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy e)
        {
            if (sender == m_mainDomain?.ScriptDomain && m_secondaryDomain == null && !m_unloadingDomain)
            {
                StartDomain($"DiscordDomain_{Guid.NewGuid().ToString()}", ref m_secondaryDomain);
                SendProxyMessage(m_secondaryDomain.ScriptDomain, new DiscordInitaliseDomainEvent(m_commandLineArgs, m_appStart));
                SendProxyMessage(sender, new DiscordDomainEvent(ProxyMessageType.RequestUnloadServer));
            }
            else if(sender == m_secondaryDomain?.ScriptDomain && m_mainDomain == null && !m_unloadingDomain)
            {
                //Should never really get hear as when it unloads main secondary becomes main;
                StartDomain($"DiscordDomain_{Guid.NewGuid().ToString()}", ref m_mainDomain);
                SendProxyMessage(m_secondaryDomain.ScriptDomain, new DiscordInitaliseDomainEvent(m_commandLineArgs, m_appStart));
                SendProxyMessage(sender, new DiscordDomainEvent(ProxyMessageType.RequestUnloadServer));
            }
            else
            {
                Logging.LogError(LogType.Client, "Failed to try and unload domain as both domains are active");
                SendProxyMessage(sender, new DiscordDomainEvent(ProxyMessageType.FailedShuttingDown, "Failed to try and unload domain as both domains are active"));
            }    
        }

        private void UnloadServerEvent(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy e)
        {
            if (sender == m_mainDomain?.ScriptDomain && m_secondaryDomain.ScriptDomain != null)
            {
                DiscordServerDomainEvent serverEvent = (DiscordServerDomainEvent)e;

                SendProxyMessage(m_secondaryDomain.ScriptDomain,    new DiscordServerDomainEvent(serverEvent.ServerName, serverEvent.ServerId, false));
                SendProxyMessage(m_mainDomain.ScriptDomain,         new DiscordDomainEvent(ProxyMessageType.RequestUnloadServer));
            }
            else if (sender == m_secondaryDomain?.ScriptDomain && m_mainDomain.ScriptDomain != null)
            {
                DiscordServerDomainEvent serverEvent = (DiscordServerDomainEvent)e;

                SendProxyMessage(m_mainDomain.ScriptDomain,         new DiscordServerDomainEvent(serverEvent.ServerName, serverEvent.ServerId, false));
                SendProxyMessage(m_secondaryDomain.ScriptDomain,    new DiscordDomainEvent(ProxyMessageType.RequestUnloadServer));
            }
            else
            {
                Logging.LogError(LogType.Client, "Failed to unload server as there is no server to load it back onto");
                SendProxyMessage(sender, new DiscordDomainEvent(ProxyMessageType.FailedUnloadServer, "Failed to try and unload domain as both domains are active"));
            }
        }

        private void UnloadDomain(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy e)
        {
            if (sender == m_mainDomain?.ScriptDomain && m_secondaryDomain.ScriptDomain != null)
            {
                UnloadDomain(m_mainDomain.Domain);
                m_mainDomain        = m_secondaryDomain;
                SendProxyMessage(m_mainDomain.ScriptDomain, new DiscordDomainEvent(ProxyMessageType.RestartComplete));
                m_secondaryDomain   = null;
            }
            else if (sender == m_secondaryDomain?.ScriptDomain && m_mainDomain.ScriptDomain != null)
            {
                UnloadDomain(m_secondaryDomain.Domain);
                SendProxyMessage(m_mainDomain.ScriptDomain, new DiscordDomainEvent(ProxyMessageType.RestartComplete));
                m_secondaryDomain = null;

            }
            else
            {
                Logging.LogError(LogType.Client, "Failed to try and unload domain as offloading script domain is invalid");
                SendProxyMessage(sender, new DiscordDomainEvent(ProxyMessageType.FailedUnloading, "Failed to try and unload domain as both domains are active"));
            }
        }

        private void UnloadDomain(AppDomain domain)
        {
            m_unloadingDomain = true;

            Thread unloadThread = new Thread(() =>
            {
                Thread.Sleep(10000);
                try
                {
                    AppDomain.Unload(domain);
                    domain = null;
                    m_unloadingDomain = false;
                    GC.Collect();
                }
                catch (Exception ex)
                {
                    if (domain != null)
                        Logging.LogException(LogType.Client, ex, $"Failed to unload domain {domain.FriendlyName}");
                    else
                        Logging.LogException(LogType.Client, ex, $"Failed to unload domain as it was null");
                }
            });
            unloadThread.Start();
        }
    }
}
