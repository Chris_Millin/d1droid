﻿using DiscordEngine.Helpers;
using System;
using System.Diagnostics;

namespace D1Droid
{
    /* 
        To-Do
        -----

        Server Unloading (DONE)
        Server Reloading (DONE)

        Admin controls (HALF DONE)
        Build and load script into server/server

        Scripts with no arguments
        flattern arguments in scripts to get or types

        Correct logging 

        Update API (DONE)

        Possible friends management
    */

    /* Initalisation Sequence

        1) Start Base domain client (that is this program)
        2) Start A new Script Domain object
        3) Initalise the new script domain to connect to discord and create the script manager
        4) Send message to tell it to load all servers (if connected do it right away if not hook it into the onconnect event on the discord client)
        5) Server will register with script manager
        6) script manager will check the servers scripts and load any it doesnt have (will compile if they are new uncahced scripts)
        7) ... run program as normal

        ..
        8) a script is requested to be removed
        9) Message Base domain saying to unload the script domain
        10) starts a second script domain
        11) tell the first domain to unload a server
        12) server is unloaded and base domain notified
        13) tells second domain to load that server (will do so if connected otherwise it will queue it up)
        14) repeats 11-13 until no more server to unload
        15) first domain is unloaded
        16) second domain becomes the main domain
        ... run program as normal
    */

    class Program
    {
        static int Main(string[] args)
        {
            CommandLineArgs cmdArgs = new CommandLineArgs(args);

            //string email    = cmdArgs.GetArgument("-email");
            //string pass     = cmdArgs.GetArgument("-pass");
            //if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(pass))
            //{
            //    Logging.LogError(LogType.Client, "Bot's Email or Password was invalid");
            //    //WaitAnyKey();
            //    return 2;
            //}

            string token = cmdArgs.GetArgument("-token");
            if(string.IsNullOrEmpty(token))
            {
                Logging.LogError(LogType.Client, "Bot's token was not set");
                return 2;
            }

            D1DiscordClient client = new D1DiscordClient(cmdArgs);

            try
            {
                client.StartClient();
            }
            catch(Exception ex)
            {
                Logging.LogException(LogType.Client, ex, "Failed to start D1 Discord Client.");
                client.StopClient();
            }
            
            while(WaitAnyKey())
            {
                System.Threading.Thread.Sleep(5000);
            }

            return 0;
        }

        public static bool WaitAnyKey()
        {
            Logging.LogInfo(LogType.Client, "Type 'exit' to quit...");
            string text = Console.ReadLine();
            if (text.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
                return true;

            return false;
        }
    }
}
