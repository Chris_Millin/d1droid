﻿using D1Droid.Script;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using DiscordSharp.Objects;
using DiscordSharp;

namespace D1Droid.Script
{
    public class HelpScript : DiscordCSharpScript
    {
        string m_helpString = string.Format(
                "\n`UUUUUUUNNNNNNNNNNLLLLLLLLLIIIIIIIIIMMMMMMMIIIIIITTTTTTEEEEEEEEDDDDDDDDDDD POWWWWWWWWWWWEEEEEEERRRRRRRR!!`\n" +
                "Commands:\n" +
                "    Killing Floor Server Management (add maps, restart server, set password)\n" +
                "`!KF2 [map|restart|password] [workshoplink|password]`\n" +
                "    SUMMRY of chat set amount of lines to return with 1 - 5\n" +
                "`!SUMMRY [lines]` \n");

        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("help", MessageRecieved);
            ActiveClient.RegisterCommand("help", PrivateMessageRecieved);
            ActiveClient.RegisterCommand("help me", MessageRecieved);
        }

        public void MessageRecieved(object sender, DiscordMessageEventArgs e)
        {
            if (e.message.content.Contains("me"))
            {
                e.author.SendMessage(m_helpString);
            }
            else
            {
                e.Channel.SendMessage(m_helpString);
            }
        }

        public void PrivateMessageRecieved(object sender, DiscordPrivateMessageEventArgs e)
        {
            e.author.SendMessage(m_helpString);
        }
    }
}
