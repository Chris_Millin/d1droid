﻿using D1Droid.Script;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

//ASM:Newtonsoft.Json.dll
namespace D1DroidEngine.Scripts
{
    public class NewVegas : DiscordCSharpScript
    {
        private struct level
        {
            public int hourMin;
            public int hourMax;
            public string levelText;
        }

        List<level> m_level = new List<level>();

        public NewVegas()
        {
            AddLevel(1600, 1610, "Level: Prestige 3");
            AddLevel(1610, 1620, "Level: Wasteland dweller");
            AddLevel(1620, 1630, "Level: ");
        }

        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("nv", MessageRecieved);
        }

        public void MessageRecieved(object sender, DiscordMessageEventArgs e)
        {
            string hours = GetHoursPlayed();
            string achievement = GetLevel(hours);

            string message = string.Format("Adam has played Fallout: New Vegas for {0}h {1}", hours, achievement);

            e.Channel.SendMessage(message);
        }


        public string GetHoursPlayed()
        {
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString("http://steamcommunity.com/id/mikemareen/games/?tab=all&sort=name");
                foreach (string line in ReadLines(htmlCode))
                {
                    if (line.StartsWith("var rgGames = "))
                    {
                        string json = string.Empty;
                        json = line.Replace("var rgGames = ", "{'games':");
                        if(json.EndsWith(";"))
                        {
                            json = json.Replace(';', '}');
                        }

                        dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                        foreach(var item in obj.games)
                        {
                            if(item.name == "Fallout: New Vegas")
                            {
                                return item.hours_forever;
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetLevel(string hours)
        {
            hours = hours.Replace(",", "");
            int hoursInt = 0;
            if (int.TryParse(hours, out hoursInt))
            {
                foreach(level lv in m_level)
                {
                    if(hoursInt < lv.hourMax && hoursInt >= lv.hourMin)
                    {
                        return lv.levelText;
                    }
                }
            }

            return string.Empty;
        }

        private void AddLevel(int min, int max, string level)
        {
            level lv = new NewVegas.level();
            lv.hourMin = min;
            lv.hourMax = max;
            lv.levelText = level;
            m_level.Add(lv);
        }

        private IEnumerable<string> ReadLines(string text)
        {
            string line = string.Empty;
            using (StringReader reader = new StringReader(text))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line.Trim();
                }
            }
        }
    }
}
