﻿using DiscordDomain.CSharp;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordDomain.CSharp
{
    public class blank : DiscordScript
    {
        const int max = 20;

        const string[] c_answers = {

            "It is certain",
            "It is decidedly so",
            "Without a doubt",
            "Yes, definitely",
            "You may rely on it",
            "As I see it, yes",
            "Most likely",
            "Outlook good",
            "Yes",
            "Signs point to yes",
            "Reply hazy try again",
            "Ask again later",
            "Better not tell you now",
            "Cannot predict now",
            "Concentrate and ask again",
            "Don't count on it",
            "My reply is no",
            "My sources say no",
            "Outlook not so good",
            "Very doubtful"

        }

        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("8ball", "shake" "Shake the 8ball", Shake);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public void Shake(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            Random rand = new Random();
            int id = rand.Next(max)
            e.Channel.SendMessage("The 8ball says `" + c_answers[id] + "`");
        } 
    }
}
