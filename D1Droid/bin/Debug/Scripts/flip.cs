﻿using DiscordDomain.CSharp;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordDomain.CSharp
{
    public class Flip : DiscordScript
    {
        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("flip", "Flips a coin", Flip)
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public void Flip(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            Random rand = new Random();
            if(rand.Next() % 2 == 0)
                e.Channel.SendMessage("Heads");
            else
                e.Channel.SendMessage("Tails");
        }        
    }
}
