﻿using D1DroidEngine.Helper;
using DiscordDomain.Admin;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Events;
using DiscordSharp.Objects;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using VideoLibrary;

//ASM:libvideo.dll,NAudio.Vorbis.dll,NAudio.dll
namespace DiscordDomain.CSharp
{
    public class Play : DiscordScript
    {
        public struct PlayInfo
        {
            public string clip;
            public DiscordChannel channel;
        }

        public Dictionary<string, string> m_clips   = new Dictionary<string, string>();
        public Dictionary<string, string> m_intros  = new Dictionary<string, string>();
        public Queue<PlayInfo> m_clipQueue = new Queue<PlayInfo>();
        public object m_mutex = new object();

        public const string c_clipFolder = "audio_clips";
        public const string c_clipStore  = "play_clip_dictionary.xml";
        public const string c_introStore = "play_intro_dictionary.xml";

        public DiscordChannel m_currentChannel;
        public bool m_consume;

        public Thread m_consumerThread;

        public Play()
        {
            
        }

        public override void RegisterCommands()
        {
            Initalise();
            //ActiveClient.RegisterCommand("play", "list", );
            ActiveClient.RegisterCommand("play", "[(add <clip>) | remove | (update <clip>)] <name>", "!(add <clip> | remove | update <clip>) <name>", PlayClip);
            ActiveClient.RegisterCommand("play", "intro (set <channel> <clip>| remove)", "!play intro (set <clip>| remove)", Intro);
            ActiveClient.RegisterEvent<DiscordVoiceStateUpdateEventArgs>(DiscordEventType.VoiceStateUpdated, CheckIntro);
        }

        public void Initalise()
        {
            string dataFolder = Path.Combine(ActiveClient.DataLocation, c_clipFolder);
            if (!Directory.Exists(dataFolder))
            {
                Directory.CreateDirectory(dataFolder);
            }

            LoadExistingClips();
            LoadExistingIntros();
            m_consumerThread = new Thread(ConsumerThread);
            m_consumerThread.Start();
        }

        public void LoadExistingClips()
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if(File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                foreach(XElement clip in clipStoreXml.Root.Elements())
                {
                    string name = string.Empty;
                    string tag = string.Empty;
                    if (clip.Attribute("name") != null)
                        name = clip.Attribute("name").Value;
                    if (clip.Attribute("tag") != null)
                        tag = clip.Attribute("tag").Value;

                    if(!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(tag))
                    {
                        try
                        {
                            m_clips.Add(tag, name);
                        }
                        catch
                        {
                            using (DiscordDebugLogger logger = new DiscordDebugLogger())
                            {
                                Logging.LogError(LogType.Client, "Cant add clip " + name + " as tag " + tag + " has already been registered");
                            }
                        }
                    }
                }
            }
            else
            {
                clipStoreXml = new XDocument();
                XElement root = new XElement("AudioClipStore");
                clipStoreXml.Add(root);
                clipStoreXml.Save(clipStore);
            }
        }

        public void LoadExistingIntros()
        {
            XDocument introStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                introStoreXml = XDocument.Load(clipStore);
                foreach (XElement clip in introStoreXml.Root.Elements())
                {
                    string name = string.Empty;
                    string user = string.Empty;
                    if (clip.Attribute("name") != null)
                        name = clip.Attribute("name").Value;
                    if (clip.Attribute("user") != null)
                        user = clip.Attribute("user").Value;

                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(user))
                    {
                        try
                        {
                            m_clips.Add(user, name);
                        }
                        catch
                        {
                            using (DiscordDebugLogger logger = new DiscordDebugLogger())
                            {
                                Logging.LogError(LogType.Client, "Cant add clip " + name + " as user " + user + " has already been registered");
                            }
                        }
                    }
                }
            }
            else
            {
                introStoreXml = new XDocument();
                XElement root = new XElement("AudioIntroStore");
                introStoreXml.Add(root);
                introStoreXml.Save(clipStore);
            }
        }

        public void AddClip(string file, string triggerName)
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                XElement clip = new XElement("AudioClip");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("tag", triggerName));
                clipStoreXml.Root.Add(clip);
                clipStoreXml.Save(clipStore);
            }
            else
            {
                clipStoreXml = new XDocument();
                XElement root = new XElement("AudioClipStore");
                XElement clip = new XElement("AudioClip");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("tag", triggerName));
                root.Add(clip);
                clipStoreXml.Add(root);
                clipStoreXml.Save(clipStore);
            }
        }

        public void AddIntro(string file, string userID)
        {
            XDocument introStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                introStoreXml = XDocument.Load(clipStore);
                XElement clip = new XElement("AudioIntro");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("user", userID));
                introStoreXml.Root.Add(clip);
                introStoreXml.Save(clipStore);
            }
            else
            {
                introStoreXml = new XDocument();
                XElement root = new XElement("AudioIntroStore");
                XElement clip = new XElement("AudioIntro");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("user", userID));
                root.Add(clip);
                introStoreXml.Add(root);
                introStoreXml.Save(clipStore);
            }
        }

        public void PlayClip(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            DiscordChannel playbackChannel = e.Author.CurrentVoiceChannel;
            string name = string.Empty;
            if (!parameters.Contains("<name>"))
            {
                e.Channel.SendMessage("Err: argument <name> is missing cant add/update/remove or play the clip");
                return;
            }
            else
            {
                ValueObject param = (ValueObject)parameters["<name>"];
                name = (string)param.Value;
            }

            if (parameters.Contains("add"))
            {
                if(m_clips.ContainsKey(name))
                {
                    e.Channel.SendMessage("Err: name " + name + " is already in uses");
                    return;
                }

                string clip = string.Empty;
                if (!parameters.Contains("<clip>"))
                {
                    ValueObject param = (ValueObject)parameters["<clip>"];
                    clip = (string)param.Value;
                }

                if (!clip.EndsWith(".mp3"))
                {
                    e.Channel.SendMessage("Err: Cant add fine as its not in .mp3 format");
                    return;
                }
                else
                {
                    using (WebClient client = new WebClient())
                    {
                        string saveLocation = Path.Combine(ActiveClient.DataLocation, c_clipFolder, name + ".mp3");
                        client.DownloadFile(clip, saveLocation);
                        AddClip(saveLocation, name);
                        m_clips.Add(name, saveLocation);
                    }
                }

            }

            else if (parameters.Contains("remove"))
            {

            }
            else if (parameters.Contains("update"))
            {

            }
            else
            {
                if(m_clips.ContainsKey(name))
                {
                    PlayInfo info = new PlayInfo();
                    info.clip = m_clips[name];
                    if(e.Author.CurrentVoiceChannel != null)
                    {
                        info.channel = e.Author.CurrentVoiceChannel;
                    }
                    else
                    {
                        info.channel = ActiveClient.Server.Channels.First(c => c.Type == ChannelType.Voice);
                    }

                    lock(m_mutex)
                    {
                        m_clipQueue.Enqueue(info);
                    }
                }
            }
        }

        public void Intro(ListDictionary parameters, DiscordMessageEventArgs e)
        {

        }

        public void CheckIntro(ListDictionary parameters, DiscordVoiceStateUpdateEventArgs e)
        {

        }

        #region Playback

        public void ConsumerThread()
        {
            bool keepConsuming = true;
            while(keepConsuming)
            {
                PlayInfo info;
                lock (m_mutex)
                {
                    info = m_clipQueue.Dequeue();
                    if(m_currentChannel == null || info.channel != m_currentChannel)
                    {
                        ConnectToChannel(info.channel);
                        m_currentChannel = info.channel;
                    }

                    //ActiveClient.Client.DisconnectFromVoice()
                }

                DiscordVoiceClient vc = ActiveClient.Client.GetVoiceClient();
                try
                {
                    int ms = vc.VoiceConfig.FrameLengthMs;
                    int channels = vc.VoiceConfig.Channels;
                    int sampleRate = 48000;

                    int blockSize = 48 * 2 * channels * ms; //sample rate * 2 * channels * milliseconds
                    byte[] buffer = new byte[blockSize];
                    var outFormat = new WaveFormat(sampleRate, 16, channels);

                    vc.SetSpeaking(true);
                    using (var mp3Reader = new MediaFoundationReader(info.clip))
                    {
                        using (var resampler = new MediaFoundationResampler(mp3Reader, outFormat) { ResamplerQuality = 20 })
                        {
                            int byteCount;
                            while ((byteCount = resampler.Read(buffer, 0, blockSize)) > 0)
                            {
                                if (vc.Connected)
                                {
                                    vc.SendVoice(buffer);
                                }
                                else
                                    break;
                            }

                            resampler.Dispose();
                            mp3Reader.Close();
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    try
                    {
                        using (DiscordDebugLogger logger = new DiscordDebugLogger())
                        {
                            Logging.LogException(LogType.Client, ex, "Failed to decode audio file");
                        }
                    }
                    catch { }
                }

                lock (m_mutex)
                {
                    keepConsuming = m_consume;
                }
            }
        }

        public bool ConnectToChannel(DiscordChannel channel)
        {
            if (ActiveClient.Server != null && channel != null)
            {
                DiscordVoiceConfig config = new DiscordVoiceConfig
                {
                    FrameLengthMs = 60,
                    Channels = 1,
                    OpusMode = DiscordSharp.Voice.OpusApplication.MusicOrMixed,
                    SendOnly = true
                };

                ActiveClient.Client.ConnectToVoiceChannel(channel, config);

                return true;
            }

            else return false;
        }

        #endregion
    }

}
