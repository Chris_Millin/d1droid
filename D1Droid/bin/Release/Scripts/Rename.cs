﻿using D1Droid.Script;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D1DroidEngine.Scripts
{
    public class Rename : DiscordCSharpScript
    {
        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("Bot", MessageRecieved);
        }

        public void MessageRecieved(object sender, DiscordMessageEventArgs e)
        {
            string message = e.message_text.Replace("!Bot ", "");
            if(message.StartsWith("rename "))
            {
                message = message.Replace("rename", "");
                if(message.Length < 30)
                {
                    ActiveClient.Client.ChangeClientUsername(message + "BOT");
                }
            }
        }
    }
}
