﻿using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using DiscordSharp.Objects;

namespace DiscordDomain.CSharp
{
    public class UserMsg : DiscordScript
    {
        string[] c_motivMsg = {
            "It is easier to Meme on others than on ourselves. - *Francois De La Rochefoucauld*",
            "Memes are better than gold or silver. - *German Proverb*",
            "Memes teach us more than books. - *Berthold Auerbach*",
            "No man will never Meme by chance. - *Seneca*",
            "Do not seek to follow in the footsteps of the men of old Memes; seek what they sought. - *Matsuo Basho*",
            "By associating with Memes you will become a Meme yourself. - *Menander*",
            "The surest way not to fail is to determine to Meme. - *Richard Brinsley Sheridan*",
            "The secret of success is to know Memes nobody else knows. - *Aristotle Onassis*",
            "He turns not back who is bound to a Meme star. - *Leonardo Da Vinci*",
            "Great Memes have always encountered violent opposition from mediocre minds. - *Albert Einstein*"
        };

        Random m_rand = new Random((int)DateTime.Now.Ticks);


        public override void RegisterCommands()
        {
            ActiveClient.RegisterEvent<DiscordGuildMemberAddEventArgs>(DiscordEventType.MemeberAdded, MemberAdded);
            ActiveClient.RegisterEvent<DiscordGuildMemberRemovedEventArgs>(DiscordEventType.MemeberRemoved, MemberRemoved);
        }

        public void MemberAdded(ListDictionary parameters, DiscordGuildMemberAddEventArgs e)
        {
            int rand = m_rand.Next(0, c_motivMsg.Length);

            foreach(DiscordChannel channel in e.Guild.Channels)
            {
                channel.SendMessage(e.AddedMember.Username + " Joins " + e.Guild.Name + "\n\n ```" + c_motivMsg[rand] + "```");
            }
        }

        public void MemberRemoved(ListDictionary parameters, DiscordGuildMemberRemovedEventArgs e)
        {
            foreach (DiscordChannel channel in e.Server.Channels)
            {
                channel.SendMessage(@"Yeah fuck off " + e.MemberRemoved.Username + @" you little cunt no one wanted to chat to you anyway '(╯°Д°）╯︵ /(.□ . \)'");
            }
        }

        public override void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}