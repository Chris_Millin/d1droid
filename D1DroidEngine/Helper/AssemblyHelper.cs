﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace D1Droid.Helper
{
    public static class AssemblyHelper
    {
        public static IEnumerable<Type> GetTypesImplementing<T>(this Assembly assembly)
        {
            return assembly.GetTypes().Where(type => type.IsClass && typeof(T).IsAssignableFrom(type));
        }
    }
}
