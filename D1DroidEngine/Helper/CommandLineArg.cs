﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D1Droid
{
    public class CommandLineArgs : MarshalByRefObject
    {
        private static string s_commandKey = "-";
        private Dictionary<string, string> m_commandLineArguments = new Dictionary<string, string>();

        public CommandLineArgs(string[] arguments)
        {
            ParseArguments(arguments);
        }

        public string GetArgument(string argumentKey)
        {
            string value = string.Empty;
            if (m_commandLineArguments.ContainsKey(argumentKey))
            {
                value = m_commandLineArguments[argumentKey];
            }

            return value;
        }

        public T GetArgument<T>(string argumentKey)
        {
            string value = string.Empty;
            if(m_commandLineArguments.ContainsKey(argumentKey))
            {
                value = m_commandLineArguments[argumentKey];
            }

            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(value);
            }
            catch
            {
                return default(T);
            }
        }

        public bool HasArgument(string argumentKey)
        {
            return m_commandLineArguments.ContainsKey(argumentKey);
        }

        private void ParseArguments(string[] arguments)
        {
            for(int index = 0; index < arguments.Length; index++)
            {
                if(arguments[index].StartsWith(s_commandKey))
                {
                    string argumentKey = arguments[index];
                    if(index + 1 < arguments.Length)
                    {
                        if(!arguments[index + 1].Contains(s_commandKey))
                        {
                            m_commandLineArguments.Add(argumentKey, arguments[++index]);
                        }
                        else
                        {
                            m_commandLineArguments.Add(argumentKey, "true");
                        }
                    }
                }
            }
        }
    }
}
