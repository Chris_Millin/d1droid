﻿using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordEngine.Helpers
{
    public enum LogType
    {
        Discord,
        ScriptEngine,
        Script,
        Server,
        Client //for anything else

    }

    public enum LogLevel
    {
        Info,
        Warning,
        Error
    }


    public delegate void LogHandler(string message);

    public class Logging
    {
        public static event LogHandler OnLog;

        public static void LogInfo(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Info, message, obj);
        }

        public static void LogWarn(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Warning, message, obj);
        }

        public static void LogError(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Error, message, obj);
        }

        public static void LogInfo(LogType type, string message)
        {
            Log(type, LogLevel.Info, message);
        }

        public static void LogWarn(LogType type, string message)
        {
            Log(type, LogLevel.Warning, message);
        }

        public static void LogError(LogType type, string message)
        {
            Log(type, LogLevel.Error, message);
        }

        public static void LogException(LogType type, Exception ex, string message)
        {
            Log(type, LogLevel.Error, $"{message}\n\n`Exception: {ex.Message}`\n\n```Stack: {ex.StackTrace}\n```");
        }

        public static void Log(LogType type, LogLevel level, string message)
        {
            Log(string.Format("[{0}] <{1}> : ", type.ToString(), level.ToString()) + message);
        }

        public static void Log(LogType type, LogLevel level, string message, params object[] obj)
        {
            Log(string.Format("[{0}] <{1}> : ", type.ToString(), level.ToString()) + string.Format(message, obj));
        }

        private static void Log(string message)
        {
            if (OnLog != null)
                OnLog(message);

            Console.WriteLine(message);
        }
    }
}
