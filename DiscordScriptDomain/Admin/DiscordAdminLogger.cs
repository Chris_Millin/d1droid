﻿using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordDomain.Admin
{
    public class DiscordAdminLogger : IDisposable
    {
        private static bool m_hooked = false;
        private static DiscordMember m_adminMember;

        public DiscordAdminLogger()
        {
            if (!m_hooked)
            {
                Logging.OnLog += LogToAdmin;
                m_hooked = true;
            }
        }

        public void Dispose()
        {
            if (m_hooked)
            {
                Logging.OnLog -= LogToAdmin;
                m_hooked = false;
            }
        }

        public static bool SetAdmin(DiscordClient client, string userID)
        {
            if (string.IsNullOrEmpty(userID))
                return false;

            foreach(var server in client.GetServersList())
            {
                foreach(var member in server.Members)
                {
                    if(member.Value.ID.Equals(userID, StringComparison.InvariantCultureIgnoreCase))
                    {
                        m_adminMember = member.Value;
                        return true;
                    }
                }
            }

            return false;
        }

        private void LogToAdmin(string message)
        {
            if(m_adminMember != null)
            {
                m_adminMember.SendMessage(message);
            }
        }
    }

    public class DiscordDebugLogger : IDisposable
    {
        private static bool m_hooked = false;
        private static DiscordChannel m_debugChannel;

        public DiscordDebugLogger()
        {
            if (!m_hooked)
            {
                Logging.OnLog += LogToDebug;
                m_hooked = true;
            }
        }

        public void Dispose()
        {
            if (m_hooked)
            {
                Logging.OnLog -= LogToDebug;
                m_hooked = false;
            }
        }

        public static bool SetChannel(DiscordClient client, string channelID)
        {
            if (string.IsNullOrEmpty(channelID))
                return false;

            foreach (var server in client.GetServersList())
            {
                if (server.ID.Equals(channelID, StringComparison.InvariantCultureIgnoreCase) ||
                    server.Name.Equals(channelID, StringComparison.InvariantCultureIgnoreCase))
                {
                    m_debugChannel = server.Channels.FirstOrDefault(c => c.Type == ChannelType.Text);
                    return true;
                }
            }

            return false;
        }

        private void LogToDebug(string message)
        {
            if (m_debugChannel != null)
            {
                m_debugChannel.SendMessage(message);
            }
        }
    }
}
