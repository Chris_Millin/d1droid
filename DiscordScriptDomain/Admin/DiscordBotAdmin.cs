﻿using D1DroidEngine.Helper;
using DiscordDomain;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DiscordDomain.Admin
{
    /// <summary>
    /// Set up and add admin related commands that will only be provided to
    /// the host of the bot
    /// 
    /// this is all a bit hacky restart is the worst
    /// </summary>
    public class DiscordBotAdmin : IDisposable
    {
        #region Private Members

        public static DiscordMember Admin
        {
            get { return m_admin; }
        }

        private readonly string m_adminUserID;

        private DiscordClient m_activeClient;
        private static DiscordMember m_admin;
        private DiscordScriptEngine m_scriptEngine;
        private DiscordScriptDomain m_scriptDomain;

        private Dictionary<DiscordMessageRule, Action<DiscordMessageRule, DiscordPrivateMessageEventArgs>> m_adminCommands;

        private const string c_adminHelpScript =
                "\n`Admin Control!`\n" +
                "Commands:\n";

        #endregion

        public DiscordBotAdmin(string userID, DiscordClient client, DiscordScriptEngine scriptEngine, DiscordScriptDomain domain) 
        {
            m_adminUserID = userID;
            m_activeClient = client;
            m_scriptDomain = domain;
            m_scriptEngine = scriptEngine;

            RegisterListeners();
        }


        public void Dispose()
        {
            if(m_activeClient != null)
            {
                m_activeClient.PrivateMessageReceived   -= activeClient_PrivateMessageReceived;
                m_activeClient.GuildCreated             -= activeClient_GuildCreated;

                m_admin         = null;
                m_scriptEngine  = null;
                m_scriptDomain  = null;
            }
        }

        #region Private Methods

        private void RegisterListeners()
        {
            m_adminCommands = new Dictionary<DiscordMessageRule, Action<DiscordMessageRule, DiscordPrivateMessageEventArgs>>
            {
                { new DiscordMessageRule("admin", "help", "`!admin help`\n    Shows this menu.\n"), Help },
                { new DiscordMessageRule("admin", "uptime", "`!admin uptime`\n    How long the bot has been running for.\n"), Uptime },
                { new DiscordMessageRule("admin", "change (name <name> | avatar | playing <text>)", "`!admin change (name <name> | avatar | playing <text>)`\n    Change info about bot (Attach image for avatar).\n"), Rename },
                { new DiscordMessageRule("admin", "join <address>", "`!admin join <address>`\n    Join a server.\n"), Join },
                { new DiscordMessageRule("admin", "leave <server>", "`!admin leave <server>`\n    Leave a server that the bot is a member of.\n"), Leave },
                { new DiscordMessageRule("admin", "list (servers [<name> users]| scripts [<server>])", "`!admin list (servers [<name> users]| scripts [<server>])`\n    List all the server the bot is a member of or all the scripts loaded.\n"), List},
                { new DiscordMessageRule("admin", "script add [<server>]", "`!admin script add [<server>]`\n    Add attached .cs scripts to all servers or specific server.\n"), AddScript },
                { new DiscordMessageRule("admin", "script remove <script> [<server>]", "`!admin script remove <script> [<server>]`\n    Removes a script from the bot (will cause a restart if removed).\n"), RemoveScript },
                { new DiscordMessageRule("admin", "restart", "`!admin restart`\n    Restarts the bot\n"), Restart },
            };


            m_activeClient.PrivateMessageReceived   += activeClient_PrivateMessageReceived;
            m_activeClient.GuildCreated             += activeClient_GuildCreated;
        }

        private void activeClient_GuildCreated(object sender, DiscordSharp.Events.DiscordGuildCreateEventArgs e)
        {
            LogAdmin($"Joining server '{e.Server.Name} - {e.Server.ID}'");
            m_scriptDomain.BuildSingleServer(e.Server.ID);
        }

        private void activeClient_PrivateMessageReceived(object sender, DiscordPrivateMessageEventArgs e)
        {
            if (e.Author.ID == m_adminUserID)
            {
                m_admin = e.Author;
                foreach (var kvp in m_adminCommands)
                {
                    if(kvp.Key.ValidatePrivateMessage(e))
                    {
                        try
                        {
                            kvp.Value(kvp.Key, e);
                        }
                        catch(Exception ex)
                        {
                            LogAdmin($"Something went wrong executing and admin command. {ex.Message}");
                        }
                    }
                }
            }
        }

        private void Help(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            StringBuilder helpMessage = new StringBuilder();
            {
                helpMessage.AppendLine(c_adminHelpScript);
                foreach (var kvp in m_adminCommands)
                {
                    helpMessage.AppendLine(kvp.Key.Help);
                }

                LogAdmin(helpMessage.ToString());
            }
        }

        private void Uptime(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            TimeSpan domainUptime = m_scriptDomain.GetDomainUpTime();
            LogAdmin($"Domain Uptime          `{domainUptime.Days} days {domainUptime.Hours} hours {domainUptime.Minutes} minutes`");
            TimeSpan Uptime = m_scriptDomain.GetUpTime();
            LogAdmin($"Application Uptime   `{Uptime.Days} days {Uptime.Hours} hours {Uptime.Minutes} minutes`");
        }

        private void Rename(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            if (argParams.Contains("name"))
            {
                ValueObject param = (ValueObject)argParams["<name>"];
                string name = (string)param.Value;

                DiscordUserInformation info = new DiscordUserInformation();
                info.Username   = name + "-Bot";
                info.Avatar     = m_activeClient.Me.Avatar;

                m_activeClient.ChangeClientInformation(info);
            }
            else if (argParams.Contains("avatar"))
            {
                if(evtArg.Attachments.Length == 0)
                {
                    LogAdmin("No Image attached for reading");
                    return;
                }

                using (WebClient client = new WebClient())
                {
                    using (DiscordAdminLogger log = new DiscordAdminLogger())
                    {
                        try
                        {
                            Image image = Bitmap.FromStream(client.OpenRead(new Uri(evtArg.Attachments.First().URL)));

                            DiscordUserInformation info = new DiscordUserInformation();
                            info.Username   = m_activeClient.Me.Username;
                            info.Avatar     = Utils.ImageToBase64(image);

                            m_activeClient.ChangeClientInformation(info);
                        }
                        catch (Exception ex)
                        {
                            Logging.LogException(LogType.Client, ex, "Failed to download Data file may already exist.");
                        }
                    }
                }
            }
            else if(argParams.Contains("playing"))
            {
                ValueObject param = (ValueObject)argParams["<text>"];
                string text = (string)param.Value;

                m_activeClient.UpdateCurrentGame(text, false);
            }
        }

        private void Join(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            ValueObject param = (ValueObject)argParams["<address>"];
            string inviteAddress = (string)param.Value;
            if (!string.IsNullOrEmpty(inviteAddress))
            {
                string inviteID = inviteAddress.Substring(inviteAddress.LastIndexOf('/') + 1);
                if (!string.IsNullOrEmpty(inviteID))
                {
                    m_activeClient.AcceptInvite(inviteID);
                    LogAdmin("Attempting to accept invite.");
                }
            }
        }

        private void Leave(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            ValueObject param = (ValueObject)argParams["<server>"];
            string serverParam = (string)param.Value;
            
            if (!string.IsNullOrEmpty(serverParam))
            {
                foreach (var server in m_activeClient.GetServersList().Where(s => s.Name == serverParam || s.ID == serverParam))
                {
                    LogAdmin($"Leaving server {server.Name} - {server.ID}");
                    m_activeClient.LeaveServer(server);
                }
            }
        }

        private void List(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            if (argParams.Contains("servers"))
            {
                StringBuilder sb = new StringBuilder();

                if (argParams.Contains("users"))
                {
                    string serverName = string.Empty;
                    if (argParams.Contains("<name>"))
                    {
                        ValueObject param = (ValueObject)argParams["<name>"];
                        serverName = (string)param.Value;
                    }

                    DiscordServer server = m_activeClient.GetServersList().FirstOrDefault(s => s.ID == serverName || s.Name == serverName);
                    if (server == null)
                    {
                        LogAdmin("No Server found with name or id " + serverName);
                    }
                    else
                    {
                        sb.AppendLine($"Server: `{server.Name}`");
                        sb.AppendLine();
                        sb.AppendLine("```");
                        foreach (var user in server.Members)
                        {
                            sb.AppendLine($"Name: {user.Value.Username} ID: {user.Value.ID} Email: {user.Value.Email} Roles: {string.Join(",", user.Value.Roles)}");
                        }
                        sb.AppendLine("```");

                        LogAdmin(sb.ToString());
                    }
                }
                else
                {
                    sb.AppendLine();
                    foreach (var server in m_activeClient.GetServersList())
                    {
                        sb.AppendLine($"`{server.Name} - {server.ID}` owned by: {server.Owner.Username}");
                    }

                    LogAdmin(sb.ToString());
                }
            }
            else if (argParams.Contains("scripts"))
            {
                string server = string.Empty;
                if (argParams.Contains("<server>"))
                {
                    ValueObject param = (ValueObject)argParams["<server>"];
                    server = (string)param.Value;
                }

                if (string.IsNullOrEmpty(server))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine();

                    //TEMP
                    foreach (string file in Directory.GetFiles(m_scriptEngine.ScriptFolder, "*.cs", SearchOption.AllDirectories))
                    {    
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        sb.AppendLine($"`Installed script` - {fileName}");
                    }

                    LogAdmin(sb.ToString());
                }
            }
        }

        private void AddScript(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            if (evtArg.Attachments.Length == 0)
                LogAdmin("No Scripts attached to this message so nothing to add.");

            foreach (var script in evtArg.Attachments)
            {
                using (WebClient client = new WebClient())
                {
                    using (DiscordAdminLogger log = new DiscordAdminLogger())
                    {
                        try
                        {
                            string server = string.Empty;
                            if (argParams.Contains("<server>"))
                            {
                                ValueObject param = (ValueObject)argParams["<server>"];
                                server = (string)param.Value;
                            }

                            if (string.IsNullOrEmpty(server))
                            {
                                client.DownloadFile(new Uri(script.URL), Path.Combine(m_scriptEngine.ScriptFolder, script.Filename));
                                m_scriptEngine.AddScript(Path.Combine(m_scriptEngine.ScriptFolder, script.Filename));
                                LogAdmin($"Added Script {script.Filename}");
                            }
                            else
                            {
                                LogAdmin($"Per server adding not implemented");
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.LogException(LogType.Client, ex, "Failed to download Data file may already exist.");
                        }
                    }
                }
            }
        }

        private void RemoveScript(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            ListDictionary argParams = sender.BuildParameters(evtArg);

            ValueObject param = (ValueObject)argParams["<script>"];
            string script = (string)param.Value;
            bool scriptRemoved = false;
            if (!string.IsNullOrEmpty(script))
            {
                string server = string.Empty;
                if(argParams.Contains("<server>"))
                    server = (string)argParams["<server>"];

                if (string.IsNullOrEmpty(server))
                {
                    foreach (string file in Directory.GetFiles(m_scriptEngine.ScriptFolder))
                    {
                        string fileName = Path.GetFileName(file);
                        if (fileName == script)
                        {
                            File.Delete(file);
                            LogAdmin("Script " + fileName + " removed");
                            scriptRemoved = true;
                        }
                    }
                }
                else
                {
                    LogAdmin("Server script removing is not implemented");
                }
            }

            if(scriptRemoved)
                m_scriptDomain.ReloadDomain();
        }

        private void Restart(DiscordMessageRule sender, DiscordPrivateMessageEventArgs evtArg)
        {
            m_scriptDomain.ReloadDomain();
        }

        private void LogAdmin(string message)
        {
            if(m_admin != null)
            {
                m_admin.SendMessage(message);
            }
        }

        #endregion
    }
}
