﻿using DiscordDomain.Helpers;
using System;

namespace DiscordDomain.CSharp
{
    public class DiscordScript : IDiscordScript
    {
        public DiscordScriptClient ActiveClient
        {
            get;
            set;
        }

        /// <summary>
        /// When the script is being compiled
        /// </summary>
        public virtual void RegisterCommands() { }

        public virtual void Dispose() { }
    }
}


