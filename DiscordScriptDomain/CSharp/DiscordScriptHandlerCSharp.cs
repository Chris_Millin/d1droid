﻿using DiscordDomain.Admin;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DiscordDomain.CSharp
{
    public class DiscordScriptHandlerCSharp : IScriptHandler
    {
        #region Public Members

        public string FileType
        {
            get
            {
                return ".cs";
            }
        }

        #endregion

        #region Private Members

        private static string s_requiredLibraries = "System.Xml.Linq.dll,System.Xml.dll,System.IO.dll,mscorlib.dll,System.dll,DiscordSharp.dll,D1DroidEngine.dll,System.Core.dll,System.Runtime.dll,Microsoft.CSharp.dll";

        private Dictionary<string, IEnumerable<Type>> m_cachedScripts = new Dictionary<string, IEnumerable<Type>>();
        private CSharpCodeProvider m_compiler;

        #endregion

        public DiscordScriptHandlerCSharp()
        {
            var providerOptions = new Dictionary<string, string>();
            providerOptions.Add("CompilerVersion", "v4.0");
            m_compiler = new CSharpCodeProvider(providerOptions);
    }

        #region Public Methods

        public IEnumerable<IDiscordScript> LoadScript(string script)
        {
            if (m_cachedScripts.ContainsKey(script))
            {
                return BuildScript(script);
            }
            else
            {
                bool success = false;
                try
                {
                    success = CompileScript(script);
                }
                catch(Exception ex)
                {
                    using (DiscordDebugLogger logger = new DiscordDebugLogger())
                    {
                        Logging.LogException(LogType.ScriptEngine, ex, $"Failed to compile script {script}");
                    }
                }

                if (success)
                {
                    return BuildScript(script);
                }
                else
                {
                    return new List<IDiscordScript>();
                }

            }
        }

        #endregion

        #region Private Methods

        private IEnumerable<IDiscordScript> BuildScript(string script)
        {
            List<IDiscordScript> builtScripts = new List<IDiscordScript>();
            foreach (var scriptType in m_cachedScripts[script])
            {
                IDiscordScript cscript = (IDiscordScript)Activator.CreateInstance(scriptType);
                builtScripts.Add(cscript);
            }

            return builtScripts;
        }

        private bool CompileScript(string script)
        {
            CompilerParameters parameters = new CompilerParameters
            {
                GenerateExecutable = false,
                GenerateInMemory = true,
                IncludeDebugInformation = true,
                //OutputAssembly = "scripts.dll"
            };

            parameters.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);
            parameters.ReferencedAssemblies.AddRange(s_requiredLibraries.Split(','));

            List<string> assemblies = AdditionalAssemblies(script);
            if (assemblies.Count != 0)
                parameters.ReferencedAssemblies.AddRange(assemblies.ToArray());

            var result = m_compiler.CompileAssemblyFromFile(parameters, script);
            if (result.Errors.HasErrors)
            {
                Logging.LogError(LogType.ScriptEngine, "Failed to compile script {0}", Path.GetFileName(script));
                foreach (CompilerError error in result.Errors)
                {
                    Logging.LogError(LogType.ScriptEngine, string.Format("{0} Line:{1} Column:{2} Error: {3}", error.ErrorNumber, error.Line, error.Column, error.ErrorText));
                }

                return false;
            }
            else
            {
                IEnumerable<Type> scripts = result.CompiledAssembly.GetTypes().Where(type => typeof(DiscordScript).IsAssignableFrom(type));
                m_cachedScripts.Add(script, scripts);
                Logging.LogInfo(LogType.ScriptEngine, "Compile script {0} successfully", Path.GetFileName(script));

                return true;
            }
        }

        private List<string> AdditionalAssemblies(string script)
        {
            List<string> additionalAssemblies = new List<string>();

            foreach (string line in File.ReadLines(script))
            {
                if (line.StartsWith("//ASM:"))
                {
                    string assembliesString = line.Replace("//ASM:", "");
                    string[] assemblies = assembliesString.Split(',');
                    additionalAssemblies.AddRange(assemblies);
                }
            }

            return additionalAssemblies;
        }

        #endregion
    }
}
