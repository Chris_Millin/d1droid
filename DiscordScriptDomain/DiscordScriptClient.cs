﻿using D1DroidEngine.Helper;
using DiscordDomain.Admin;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Events;
using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DiscordDomain
{

    public enum DiscordEventType
    {
        PrivateMessageRecieved,
        PrivateMessageDeleted,
        MessageRecieved,
        MessageDeleted,
        MessageEdited,
        Connected,
        RoleUpdated,
        RoleDeleted,
        MemeberAdded,
        MemeberRemoved,
        LeftVoiceChannel,
        UserSpeaking,
        UserTyping,
        UserUpdated,
        VoiceClientConnected,
        VoiceStateUpdated,
        URLUpdate
    }

    public struct DiscordEvent
    {
        public IDiscordRule Rule;
        public DiscordEventType Type;
        public string Help;
        public Delegate Event;
    }

    /// <summary>
    /// This is a Discord Client wrapper that attempts to hide
    /// things the scripter shouldnt have access to.
    /// </summary>
    public class DiscordScriptClient : IDisposable
    {
        public DiscordClient Client
        {
            get { return m_activeClient; }
        }

        public bool PrivateServer
        {
            get { return m_privateServer;  }
        }

        public DiscordServer Server
        {
            get { return m_discordServer; }
        }

        public DiscordMember Admin
        {
            get { return DiscordBotAdmin.Admin; }
        }

        public string DataLocation
        {
            get
            {
                string dataLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data");
                if (!Directory.Exists(dataLocation))
                    Directory.CreateDirectory(dataLocation);

                return dataLocation;
            }
        }

        public string DataShortLocation
        {
            get
            {
                return "Data";
            }
        }


        private const string c_help = "__**Memé-Bot Help Info 😂 👌**__\n\n" +
            "``` This is the meme menu for all your meme usages. \n" +
            "All Commands must start with a ! otherwise they will not work and they are also case sensitive (all should be in lower case)\n\n" +
            "Each argument in a in a command must be seperate by a space; in order to input a string with spaces use quotes around it e.g. !cmd say \"Hello World\"\n" +
            "This will split the command into [cmd] [say] and [Hello World].\n\n" +
            "Commands also use POSIX format argument list via docopt which can be found http://docopt.org/ \n" +
            "Basic info is anything in () means it has to use something in there. [] any command in here is optional. <> this is where you input something. Any plain text means" +
            "you have to enter it. a | between commands means you can enter one or the other.\nEnought Ballshit here are the current command list\n```" +
            "-------------------------------------------------\n\n`COMMANDS`";



        private DiscordClient m_activeClient;

        private Dictionary<DiscordEventType, List<DiscordEvent>> m_discordEvents    = new Dictionary<DiscordEventType, List<DiscordEvent>>();
        private DiscordServer m_discordServer = null;
        private bool m_privateServer = false;

        public DiscordScriptClient(DiscordClient activeClient, DiscordServer server)
        {
            m_activeClient  = activeClient;
            m_privateServer = server == null ? true : false;
            m_discordServer = server;

            RegisterCommand("help", "Displays this message", ShowHelpChan);
            RegisterCommand("help", "Displays this message", ShowHelpPriv);
            RegisterAllEvents();
        }

        #region Register Commands

        public void RegisterCommand(string command, string pattern, string help, Action<ListDictionary, DiscordMessageEventArgs> eventMethod)
        {
            DiscordMessageRule rule     = new DiscordMessageRule(command, pattern, help);
            DiscordEvent discordEvent   = new DiscordEvent();
            discordEvent.Rule           = rule;
            discordEvent.Type           = DiscordEventType.MessageRecieved;
            discordEvent.Event          = eventMethod;
            AddDiscordEvent(discordEvent);
        }

        public void RegisterCommand(string command, string help, Action<ListDictionary, DiscordMessageEventArgs> eventMethod)
        {
            DiscordMessageRule rule = new DiscordMessageRule(command, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = DiscordEventType.MessageRecieved;
            discordEvent.Event = eventMethod;
            AddDiscordEvent(discordEvent);
        }

        public void RegisterCommand(string command, string pattern, string help, Action<ListDictionary, DiscordPrivateMessageEventArgs> eventMethod)
        {
            DiscordMessageRule rule     = new DiscordMessageRule(command, pattern, help);
            DiscordEvent discordEvent   = new DiscordEvent();
            discordEvent.Rule           = rule;
            discordEvent.Type           = DiscordEventType.PrivateMessageRecieved;
            discordEvent.Event          = eventMethod;
            AddDiscordEvent(discordEvent);
        }

        public void RegisterCommand(string command, string help, Action<ListDictionary, DiscordPrivateMessageEventArgs> eventMethod)
        {
            DiscordMessageRule rule = new DiscordMessageRule(command, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = DiscordEventType.PrivateMessageRecieved;
            discordEvent.Event = eventMethod;
            AddDiscordEvent(discordEvent);
        }

        public bool RegisterEvent<T>(DiscordEventType eventType, Action<ListDictionary, T> eventMethod)
        {
            DiscordBasicRule rule = new DiscordBasicRule();
            if(rule.IsEventSupported(eventType))
            {
                DiscordEvent discordEvent = new DiscordEvent();
                discordEvent.Rule   = rule;
                discordEvent.Type   = eventType;
                discordEvent.Event  = eventMethod;
                AddDiscordEvent(discordEvent);
                return false;
            }
            return true;
        }

        public bool RegisterEvent<T>(IDiscordRule rule, DiscordEventType eventType, Action<ListDictionary, T> eventMethod)
        {
            if(rule.IsEventSupported(eventType))
            {
                DiscordEvent discordEvent = new DiscordEvent();
                discordEvent.Rule   = rule;
                discordEvent.Type   = eventType;
                discordEvent.Event  = eventMethod;
                AddDiscordEvent(discordEvent);
                return true;
            }

            return false;
        }

        #endregion

        public void Dispose()
        {
            m_activeClient.MessageReceived -= Client_MessageReceived;
            m_activeClient.MessageDeleted -= Client_MessageDeleted;
            m_activeClient.MessageEdited -= Client_MessageEdited;

            m_activeClient.PrivateMessageReceived -= Client_PrivateMessageReceived;
            m_activeClient.PrivateMessageDeleted -= Client_PrivateMessageDeleted;

            m_activeClient.RoleUpdated -= Client_RoleUpdated;
            m_activeClient.RoleDeleted -= Client_RoleDeleted;

            m_activeClient.UserAddedToServer -= Client_UserAddedToServer;
            m_activeClient.UserRemovedFromServer -= Client_UserRemovedFromServer;

            m_activeClient.UserLeftVoiceChannel -= Client_UserLeftVoiceChannel;
            m_activeClient.UserSpeaking -= Client_UserSpeaking;
            m_activeClient.UserTypingStart -= Client_UserTypingStart;
            m_activeClient.UserUpdate -= Client_UserUpdate;

            m_activeClient.VoiceClientConnected -= Client_VoiceClientConnected;
            m_activeClient.VoiceStateUpdate -= Client_VoiceStateUpdate;

            m_activeClient.URLMessageAutoUpdate -= Client_URLMessageAutoUpdate;
        }

        private void AddDiscordEvent(DiscordEvent eventAction)
        {
            DiscordEventType type = eventAction.Type;
            if (m_discordEvents.ContainsKey(type))
            {
                if (m_discordEvents[type] == null)
                {
                    m_discordEvents[type] = new List<DiscordEvent>();
                    m_discordEvents[type].Add(eventAction);
                }
                else
                {
                    m_discordEvents[type].Add(eventAction);
                }
            }
            else
            {
                m_discordEvents.Add(type, new List<DiscordEvent>());
                m_discordEvents[type].Add(eventAction);
            }
        }
        
        private void RegisterAllEvents()
        {
            m_activeClient.MessageReceived += Client_MessageReceived;
            m_activeClient.MessageDeleted += Client_MessageDeleted;
            m_activeClient.MessageEdited += Client_MessageEdited;

            m_activeClient.PrivateMessageReceived += Client_PrivateMessageReceived;
            m_activeClient.PrivateMessageDeleted += Client_PrivateMessageDeleted;

            m_activeClient.RoleUpdated += Client_RoleUpdated;
            m_activeClient.RoleDeleted += Client_RoleDeleted;

            m_activeClient.UserAddedToServer += Client_UserAddedToServer;
            m_activeClient.UserRemovedFromServer += Client_UserRemovedFromServer;

            m_activeClient.UserLeftVoiceChannel += Client_UserLeftVoiceChannel;
            m_activeClient.UserSpeaking += Client_UserSpeaking;
            m_activeClient.UserTypingStart += Client_UserTypingStart;
            m_activeClient.UserUpdate += Client_UserUpdate;

            m_activeClient.VoiceClientConnected += Client_VoiceClientConnected;
            m_activeClient.VoiceStateUpdate += Client_VoiceStateUpdate;

            m_activeClient.URLMessageAutoUpdate += Client_URLMessageAutoUpdate;

            //never going to get this message from a script due to load order
            //m_activeClient.Connected += Client_Connected;
        }

        private void ShowHelpChan(ListDictionary param, DiscordMessageEventArgs e)
        {
            DisplayHelp(e.Author);
        }

        private void ShowHelpPriv(ListDictionary param, DiscordPrivateMessageEventArgs e)
        {
            DisplayHelp(e.Author);
        }

        public void DisplayHelp(DiscordMember memeber)
        {
            memeber.SendMessage(c_help);
            if (m_discordEvents.ContainsKey(DiscordEventType.MessageRecieved) && m_discordEvents[DiscordEventType.MessageRecieved].Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Public commands:\n");
                foreach (DiscordEvent dEvent in m_discordEvents[DiscordEventType.MessageRecieved])
                {
                    if(dEvent.Rule is DiscordMessageRule)
                    {
                        DiscordMessageRule rule = dEvent.Rule as DiscordMessageRule;
                        sb.AppendLine($"`!{rule.Command} {rule.Pattern}`");
                        sb.AppendLine($"    {rule?.Help}");
                        sb.AppendLine();
                    }
                }
                memeber.SendMessage(sb.ToString());
            }
            
            if(m_discordEvents.ContainsKey(DiscordEventType.PrivateMessageRecieved) && m_discordEvents[DiscordEventType.PrivateMessageRecieved].Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Private commands:\n");
                foreach (DiscordEvent dEvent in m_discordEvents[DiscordEventType.PrivateMessageRecieved])
                {
                    if (dEvent.Rule is DiscordMessageRule)
                    {
                        DiscordMessageRule rule = dEvent.Rule as DiscordMessageRule;
                        sb.AppendLine($"`!{rule.Command} {rule.Pattern}`");
                        sb.AppendLine($"    {rule?.Help}");
                        sb.AppendLine();
                    }
                }
                memeber.SendMessage(sb.ToString());
            }
        }

        #region DiscordEvents

        private void Client_URLMessageAutoUpdate(object sender, DiscordURLUpdateEventArgs e)
        {
            try
            { 
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordURLUpdateEventArgs>(DiscordEventType.URLUpdate, e);
                }
            }
            catch (Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"URL Message Auto Update Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_VoiceStateUpdate(object sender, DiscordVoiceStateUpdateEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer.ID == e.Guild.ID)
                {
                    InvokeEvent<DiscordVoiceStateUpdateEventArgs>(DiscordEventType.VoiceStateUpdated, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Voice Status Update Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_VoiceClientConnected(object sender, EventArgs e)
        {
            try
            { 
                if (m_discordServer != null)
                {
                    InvokeEvent<EventArgs>(DiscordEventType.VoiceClientConnected, e);
                }
            }
            catch (Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Voice Client Connect Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_UserUpdate(object sender, DiscordUserUpdateEventArgs e)
        {
            try
            {
                if (m_discordServer == null)
                {
                    InvokeEvent<DiscordUserUpdateEventArgs>(DiscordEventType.UserUpdated, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Updated Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_UserTypingStart(object sender, DiscordTypingStartEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordTypingStartEventArgs>(DiscordEventType.UserTyping, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Typing Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_UserSpeaking(object sender, DiscordVoiceUserSpeakingEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordVoiceUserSpeakingEventArgs>(DiscordEventType.UserSpeaking, e);
                }
            }
            catch (Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Speaking Failed in server {Server?.Name}");
                }
            }

        }

        private void Client_UserLeftVoiceChannel(object sender, DiscordLeftVoiceChannelEventArgs e)
        {
            try
            {
                if (m_discordServer != null)
                {
                    InvokeEvent<DiscordLeftVoiceChannelEventArgs>(DiscordEventType.LeftVoiceChannel, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Left Voice Channel Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_UserRemovedFromServer(object sender, DiscordGuildMemberRemovedEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer == e.Server && e.MemberRemoved != m_activeClient.Me)
                {
                    InvokeEvent<DiscordGuildMemberRemovedEventArgs>(DiscordEventType.MemeberRemoved, e);
                }
            }
            catch (Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Removed from Server Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_UserAddedToServer(object sender, DiscordGuildMemberAddEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer == e.Guild)
                {
                    InvokeEvent<DiscordGuildMemberAddEventArgs>(DiscordEventType.MemeberAdded, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"User Added to Server Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_RoleDeleted(object sender, DiscordGuildRoleDeleteEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer == e.Guild)
                {
                    InvokeEvent<DiscordGuildRoleDeleteEventArgs>(DiscordEventType.RoleDeleted, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Role Deleted Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_RoleUpdated(object sender, DiscordGuildRoleUpdateEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer == e.InServer)
                {
                    InvokeEvent<DiscordGuildRoleUpdateEventArgs>(DiscordEventType.RoleUpdated, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Role Updated Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_PrivateMessageDeleted(object sender, DiscordPrivateMessageDeletedEventArgs e)
        {
            try
            {
                if (m_privateServer)
                {
                    InvokeEvent<DiscordPrivateMessageDeletedEventArgs>(DiscordEventType.PrivateMessageDeleted, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Private Message Deleted Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_MessageEdited(object sender, DiscordMessageEditedEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordMessageEditedEventArgs>(DiscordEventType.MessageEdited, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Message Edited Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_MessageDeleted(object sender, DiscordMessageDeletedEventArgs e)
        {
            try
            { 
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordMessageDeletedEventArgs>(DiscordEventType.MessageDeleted, e);
                }
            }
            catch (Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Message Deleted Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_MessageReceived(object sender, DiscordMessageEventArgs e)
        {
            try
            {
                if (m_discordServer != null && m_discordServer.Channels.Any(c => c.ID == e.Channel.ID))
                {
                    InvokeEvent<DiscordMessageEventArgs>(DiscordEventType.MessageRecieved, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"MessageReceived Failed in server {Server?.Name}");
                }
            }
        }

        private void Client_PrivateMessageReceived(object sender, DiscordPrivateMessageEventArgs e)
        {
            try
            {
                if (m_privateServer)
                {
                    InvokeEvent<DiscordPrivateMessageEventArgs>(DiscordEventType.PrivateMessageRecieved, e);
                }
            }
            catch(Exception ex)
            {
                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                {
                    Logging.LogException(LogType.Client, ex, $"Private Message Received Failed in server {Server?.Name}");
                }
            }
        }

        #endregion

        private bool InvokeEvent<T>(DiscordEventType eventType, EventArgs e)
        {
            T discordEventArgs = default(T);
            if (e.TryCast<T>(out discordEventArgs))
            {
                if (m_discordEvents.ContainsKey(eventType))
                {
                    bool invoked = false;
                    foreach (var eventObject in m_discordEvents[eventType])
                    {
                        if (eventObject.Type == eventType &&
                            eventObject.Rule.IsEventSupported(eventObject.Type) &&
                            eventObject.Rule.Validate(e))
                        {
                            invoked = true;

                            ListDictionary parameters = eventObject.Rule.BuildParameters(e);

                            System.Threading.ThreadPool.QueueUserWorkItem((object sender) =>
                                {
                                    try
                                    {
                                        ((Action<ListDictionary, T>)eventObject.Event)?.Invoke((ListDictionary)sender, discordEventArgs);
                                    }
                                    catch(Exception ex)
                                    {
                                        using (DiscordDebugLogger logger = new DiscordDebugLogger())
                                        {
                                            string format = string.Format("Exeption occued: {0}\nStack: {1}", ex.Message, ex.StackTrace);
                                            Logging.LogInfo(LogType.Script, format);
                                        }
                                    }

                                }, parameters);
                        }

                    }

                    return invoked;
                }
            }

            return false;
        }

        
    }
}
