﻿using D1Droid;
using DiscordDomain.Admin;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DiscordDomain
{
    public class DiscordScriptDomain : MarshalByRefObject, IDiscordDomainHandlerProxy, IDisposable
    {
        private IDiscordDomainHandlerProxy m_host;

        private DiscordBotAdmin m_botAdminControl;
        private DiscordScriptEngine m_scriptEngine;
        private DiscordClient m_activeClient;
        private Dictionary<string, DiscordServerScriptManager> m_activeServers = new Dictionary<string, DiscordServerScriptManager>();

        private const string c_privateServerId = "private";

        private string m_adminId;
        private string m_debugChannelId;

        private bool m_isConnected = false;

        private DateTime m_creationTime = DateTime.MinValue;
        private DateTime m_hostCreationTime = DateTime.MinValue;
        private Thread m_connectionThread;


        public DiscordScriptDomain(IDiscordDomainHandlerProxy host)
        {
            m_host = host;
            m_host.ProxyMessage(this, new DiscordDomainEvent(ProxyMessageType.ScriptDomainStarted));
        }

        public void InitaliseDomain(IDiscordDomainEventProxy message)
        {
            lock (m_activeServers)
            {
                if (!(message is DiscordInitaliseDomainEvent))
                {
                    Logging.LogError(LogType.Client, "Failed to initalise domain as message was not an Initalisation message.");
                    return;
                }

                DiscordInitaliseDomainEvent initMsg = (DiscordInitaliseDomainEvent)message;

                m_creationTime      = DateTime.Now;
                m_hostCreationTime  = initMsg.AppStartTime;

                CommandLineArgs cmdArgs = initMsg.CommandLineArgs;

                string token        = cmdArgs.GetArgument("-token");
                m_adminId           = cmdArgs.GetArgument("-admin");
                m_debugChannelId    = cmdArgs.GetArgument("-debugserver");

                m_activeClient = new DiscordClient(token, true, true);

                m_activeClient.Connected                += activeClient_Connected;
                m_activeClient.UserRemovedFromServer    += activeClient_UserRemovedFromServer;
                m_activeClient.GuildAvailable           += activeClient_GuildAvailable;
                m_activeClient.KeepAliveSent            += activeClient_KeepAliveSent;

                m_scriptEngine      = new DiscordScriptEngine();
                m_botAdminControl   = new DiscordBotAdmin(m_adminId, m_activeClient, m_scriptEngine, this);

                try
                {
                    m_activeClient.SendLoginRequest();
                    m_activeClient.Connect();

                    Thread waitThread = new Thread(() =>
                    {
                        while (!m_isConnected)
                        { 
                            Thread.Sleep(1000);
                        }
                    });

                    waitThread.Start();
                    if (waitThread.Join(60000))
                        waitThread.Abort();
                }
                catch(Exception ex)
                {
                    Logging.Log(LogType.ScriptEngine, LogLevel.Error, $"Failed to connect to discord with user account {token}");
                    Logging.Log(LogType.ScriptEngine, LogLevel.Error, $"Exception: {ex.Message}\nStack: {ex.StackTrace}");
                    return;
                }
            }
        }

        private void activeClient_KeepAliveSent(object sender, DiscordKeepAliveSentEventArgs e)
        {
            //if (!e.SocketAlive)
            //{
            //    m_activeClient.Logout();
            //    if (m_connectionThread != null)
            //    {
            //        m_connectionThread.Abort();
            //        m_connectionThread = null;
            //    }

            //    m_connectionThread = new Thread(() =>
            //    {
            //        ReloadDomain();
            //    });
            //    m_connectionThread.Start();
            //}

        }

        private void activeClient_GuildAvailable(object sender, DiscordSharp.Events.DiscordGuildCreateEventArgs e)
        {
            DiscordAdminLogger.SetAdmin(m_activeClient, m_adminId);
            DiscordDebugLogger.SetChannel(m_activeClient, m_debugChannelId);
        }

        public TimeSpan GetDomainUpTime()
        {
            return DateTime.Now - m_creationTime;
        }

        public TimeSpan GetUpTime()
        {
            return DateTime.Now - m_hostCreationTime;
        }

        public void ReloadDomain()
        {
            Logging.LogInfo(LogType.Client, "Shutting down domain");
            SendMessage(m_host, new DiscordDomainEvent(ProxyMessageType.RequestUnloadDomain));
        }

        private void activeClient_Connected(object sender, DiscordConnectEventArgs e)
        {
            m_isConnected = true;

            Logging.Log(LogType.ScriptEngine, LogLevel.Info, $"User {e.User.Username} connected to Discord");

            

            using (DiscordDebugLogger dbgLogger = new DiscordDebugLogger())
            {
                m_scriptEngine.LoadHandlers();
            }
        }

        private void activeClient_UserRemovedFromServer(object sender, DiscordSharp.Events.DiscordGuildMemberRemovedEventArgs e)
        { 
            if (e.MemberRemoved.ID == m_activeClient.Me.ID)
            {
                if (m_activeServers.ContainsKey(e.Server.ID))
                {
                    lock (m_activeServers)
                    {
                        m_activeServers[e.Server.ID].Dispose();
                        m_activeServers.Remove(e.Server.ID);
                    }
                }
            }
        }
        
        private void LoadAllServers()
        {
            if(m_isConnected)
            {
                BuildAllServers();
            }
            else
            {
                Logging.LogError(LogType.Client, "Cant build servers while not connected");
            }
        }

        private void BuildAllServers()
        {
            lock (m_activeServers)
            {
                DiscordServerScriptManager privateServerManager = new DiscordServerScriptManager(m_activeClient, null, m_scriptEngine);
                m_activeServers.Add(c_privateServerId, privateServerManager);

                foreach (var server in m_activeClient.GetServersList())
                {
                    if (!m_activeServers.ContainsKey(server.ID))
                    {
                        DiscordServerScriptManager serverManager = new DiscordServerScriptManager(m_activeClient, server, m_scriptEngine);
                        m_activeServers.Add(server.ID, serverManager);
                    }
                }
            }
        }

        private void LoadServer(IDiscordDomainEventProxy msg)
        {
            if (msg is DiscordServerDomainEvent)
            {
                DiscordServerDomainEvent msgEvent = (DiscordServerDomainEvent)msg;
                if (m_isConnected)
                {

                    BuildSingleServer(msgEvent.ServerId);
                }
                else
                {
                    Logging.LogError(LogType.Client, $"Cant build server {msgEvent.ServerName} while not connected");
                }
            }
        }

        public void BuildSingleServer(string serverId)
        {
            lock (m_activeServers)
            {
                if(serverId == c_privateServerId)
                {
                    DiscordServerScriptManager privateServerManager = new DiscordServerScriptManager(m_activeClient, null, m_scriptEngine);
                    m_activeServers.Add(c_privateServerId, privateServerManager);
                }
                else
                {
                    DiscordServer server = m_activeClient.GetServersList().FirstOrDefault(s => s.ID == serverId);
                    if(server != null)
                    {
                        if(m_activeServers.ContainsKey(serverId))
                        {
                            m_activeServers[serverId].Dispose();
                            m_activeServers.Remove(serverId);
                        }

                        DiscordServerScriptManager scriptServer = new DiscordServerScriptManager(m_activeClient, server, m_scriptEngine);
                        m_activeServers.Add(scriptServer.Id, scriptServer);
                    }
                }
            }
        }

        private void UnloadServer()
        {
            if (m_activeServers.Count != 0)
            {
                string serverName   = string.Empty;
                string serverId     = string.Empty;
                lock (m_activeServers)
                {
                    DiscordServerScriptManager server = m_activeServers.First().Value;
                    Logging.LogInfo(LogType.Client, $"Unloading server {server.Name}");
                    serverName  = server.Name;
                    serverId    = m_activeServers.First().Key;
                    m_activeServers.Remove(serverId);
                    server.Dispose();
                }

                SendMessage(m_host, new DiscordServerDomainEvent(serverName, serverId, true));
            }
            else
            {
                m_botAdminControl.Dispose();
                m_botAdminControl = null;
                
                m_activeClient.Connected                -= activeClient_Connected;
                m_activeClient.UserRemovedFromServer    -= activeClient_UserRemovedFromServer;
                m_activeClient.GuildAvailable           -= activeClient_GuildAvailable;
                m_activeClient.KeepAliveSent            -= activeClient_KeepAliveSent;
                m_activeClient = null;
                SendMessage(m_host, new DiscordDomainEvent(ProxyMessageType.UnloadDomain));
            }
        }

        public void ProxyMessage(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy msg)
        {
            switch(msg.ProxyMessage)
            {
                case ProxyMessageType.InitaliseDomain:
                    InitaliseDomain(msg);
                    break;
                case ProxyMessageType.RequestUnloadServer:
                    UnloadServer();
                    break;
                case ProxyMessageType.RequestLoadAllServers:
                    LoadAllServers();
                    break;
                case ProxyMessageType.RequestLoadServer:
                    LoadServer(msg);
                    break;
                case ProxyMessageType.RestartComplete:
                    using (DiscordAdminLogger logger = new DiscordAdminLogger())
                    {
                        Logging.LogInfo(LogType.Client, "Bot Client back online for all servers");
                    }
                    break;
                case ProxyMessageType.FailedShuttingDown:
                case ProxyMessageType.FailedUnloading:
                case ProxyMessageType.FailedUnloadServer:
                    DiscordDomainEvent evnt = (DiscordDomainEvent)msg;
                    using (DiscordAdminLogger logger = new DiscordAdminLogger())
                    {
                        Logging.LogInfo(LogType.Client, $"\"ProxyMessageType.{evnt.ProxyMessage.ToString()}\" {evnt.Message}");
                    }
                    break;
            }
        }


        public void Dispose()
        {
        }

        private void SendMessage(IDiscordDomainHandlerProxy reciever, IDiscordDomainEventProxy msg)
        {
            reciever.ProxyMessage(this, msg);
        }
    }
}
