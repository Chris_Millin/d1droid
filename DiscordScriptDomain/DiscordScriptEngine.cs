﻿using System;
using System.Collections.Generic;
using System.Reflection;
using D1Droid.Helper;
using System.IO;
using DiscordDomain.CSharp;
using DiscordDomain.Helpers;
using DiscordSharp;
using DiscordEngine.Helpers;

namespace DiscordDomain
{
    public interface IScriptHandler
    {
        string FileType { get; }

        IEnumerable<IDiscordScript> LoadScript(string script);
    }

    public delegate void LoadScriptHandler(IEnumerable<IDiscordScript> scripts);

    public class DiscordScriptEngine : IDisposable 
    {
        #region Public Members

        public string ScriptFolder
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), c_scriptFolder);
            }
        }

        #endregion

        #region Private Members

        private const string c_scriptFolder = "scripts";
        private List<IScriptHandler> m_scriptHandlers = new List<IScriptHandler>();

        private Dictionary<string, LoadScriptHandler> m_cachedServers = new Dictionary<string, LoadScriptHandler>();

        private object m_mutex = new object();

        #endregion

        #region Public Methods

        public void LoadHandlers()
        {
            IEnumerable<Type> handlers = Assembly.GetExecutingAssembly().GetTypesImplementing<IScriptHandler>();
            foreach (Type handlerType in handlers)
            {
                IScriptHandler handler  = (IScriptHandler)Activator.CreateInstance(handlerType);
                m_scriptHandlers.Add(handler);
            }
        }

        public void AddScript(string script)
        {
            foreach (IScriptHandler handler in m_scriptHandlers)
            {
                if (handler.FileType == Path.GetExtension(script))
                {
                    foreach (var server in m_cachedServers)
                    {
                        System.Threading.ThreadPool.QueueUserWorkItem((object sender) =>
                        {
                            IEnumerable<IDiscordScript> scripts = null;
                            lock (m_mutex)
                            {
                                scripts = handler.LoadScript(script);
                            }
                            server.Value(scripts);
                        });
                    }
                }
            }
        }

        //ADD ADDSCRIPT TO SPECIFIC SERVER
        public void RemoveScript(string serverID, string script)
        {

        }


        public void RegisterServer(DiscordServerScriptManager server, LoadScriptHandler loadScriptEvent)
        {
            if (m_cachedServers.ContainsKey(server.Id))
                Logging.LogWarn(LogType.ScriptEngine, $"Server {server.Name} already registerd with script engine.");
            else if (loadScriptEvent == null)
                Logging.LogError(LogType.ScriptEngine, $"Server {server.Name} tried to register with an invalid load script handler.");
            else
            {
                m_cachedServers.Add(server.Id, loadScriptEvent);
                Logging.LogInfo(LogType.ScriptEngine, $"Registering Server {server.Name}");
                System.Threading.ThreadPool.QueueUserWorkItem((object sender) =>
                {
                    LoadScriptHandler scriptHandler = null;
                    lock(m_mutex)
                    {
                        scriptHandler = m_cachedServers[server.Id];
                    }

                    if (m_cachedServers != null)
                    {
                        IEnumerable<IDiscordScript> baseScripts = LoadBaseScripts();
                        IEnumerable<IDiscordScript> serverScripts = LoadServerScripts(server.Id);
                        scriptHandler(baseScripts);
                        scriptHandler(serverScripts);
                    }

                }, this);
            }
        }

        public void UnregisterServer(DiscordServerScriptManager server)
        {
            if (m_cachedServers.ContainsKey(server.Id))
            {
                Logging.LogInfo(LogType.ScriptEngine, $"Unregistering Server {server.Name}");

                lock (m_cachedServers)
                {
                    m_cachedServers.Remove(server.Id);
                }
            }
        }

        public void Dispose()
        {
            /*foreach(var scriptHandler in m_scriptHandlers)
            {
                scriptHandler.
            }*/
        }

        #endregion

        #region Private Methods

        private IEnumerable<IDiscordScript> LoadBaseScripts()
        {
            List<IDiscordScript> m_builtScripts = new List<IDiscordScript>();

            foreach (string file in Directory.GetFiles(c_scriptFolder, "*.*"))
            {
                foreach (IScriptHandler handler in m_scriptHandlers)
                {
                    if (handler.FileType == Path.GetExtension(file))
                    {
                        lock (m_mutex)
                        {
                            m_builtScripts.AddRange(handler.LoadScript(file));
                        }
                    }
                }
            }

            return m_builtScripts;
        }

        private IEnumerable<IDiscordScript> LoadServerScripts(string serverId)
        {
            List<IDiscordScript> m_builtScripts = new List<IDiscordScript>();

            string serverScriptFolder = Path.Combine(c_scriptFolder, serverId);
            if(!Directory.Exists(serverScriptFolder))
            {
                Directory.CreateDirectory(serverScriptFolder);
                return m_builtScripts;
            }

            foreach (string file in Directory.GetFiles(serverScriptFolder, "*.*"))
            {
                foreach (IScriptHandler handler in m_scriptHandlers)
                {
                    if (handler.FileType == Path.GetExtension(file))
                    {
                        lock (m_builtScripts)
                        {
                            m_builtScripts.AddRange(handler.LoadScript(file));
                        }
                    }
                }
            }

            return m_builtScripts;
        }

        #endregion
    }
}
