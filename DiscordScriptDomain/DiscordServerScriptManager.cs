﻿using D1Droid;
using DiscordDomain.Helpers;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscordDomain
{
    public class DiscordServerScriptManager : IDisposable
    {

        #region Public Members

        public string Name
        {
            get
            {
                if (m_activeServer != null)
                    return m_activeServer.Name;
                else
                    return c_privateServerName;
            }
        }

        public string Id
        {
            get
            {
                if (m_activeServer != null)
                    return m_activeServer.ID;
                else
                    return c_privateServerId;
            }
        }

        #endregion

        private DiscordScriptEngine m_scriptEngine      = null;
        private DiscordScriptClient m_scriptClient      = null;
        private DiscordServer m_activeServer            = null;
        private List<IDiscordScript> m_loadedScripts    = null;

        private object m_serverMutex = new object();

        private const string c_privateServerName    = "Private Messaging Server";
        private const string c_privateServerId      = "private";

        public DiscordServerScriptManager(DiscordClient client, DiscordServer server, DiscordScriptEngine engine)
        {
            if (server != null)
                Logging.LogInfo(LogType.Client, $"Building Bot for server {server.Name}");
            else
                Logging.LogInfo(LogType.Client, $"Building Bot for private server");

            m_activeServer = server;
            m_scriptEngine = engine;
            m_scriptClient = new DiscordScriptClient(client, m_activeServer);

            m_loadedScripts = new List<IDiscordScript>();

            m_scriptEngine.RegisterServer(this, OnLoadScripts);
        }

        public void Dispose()
        { 
            m_scriptClient.Dispose();

            foreach(var script in m_loadedScripts)
            {
                script.Dispose();
            }

            m_scriptEngine.UnregisterServer(this);
        }

        private void OnLoadScripts(IEnumerable<IDiscordScript> scripts)
        {
            int scriptCount = scripts.Count();
            if (scriptCount != 0)
                Logging.Log(LogType.Server, LogLevel.Info, $"Loading {scriptCount} new scripts");

            foreach (IDiscordScript script in scripts)
            {
                script.ActiveClient = m_scriptClient;
                script.RegisterCommands();
                m_loadedScripts.Add(script);
            }
        }
    }
}
