﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;

namespace DiscordDomain.Helper
{
    /// <summary>
    /// NOTE: All this has been lifted and modified from Docopt c# library
    /// </summary>

    public class ValueObject
    {
        public object Value { get; private set; }

        internal ValueObject(object obj)
        {
            if (obj is ArrayList)
            {
                Value = new ArrayList(obj as ArrayList);
                return;
            }
            if (obj is ICollection)
            {
                Value = new ArrayList(obj as ICollection);
                return;
            }
            Value = obj;
        }

        internal ValueObject()
        {
            Value = null;
        }

        public bool IsNullOrEmpty
        {
            get { return Value == null || Value.ToString() == ""; }
        }

        public bool IsFalse
        {
            get { return (Value as bool?) == false; }
        }

        public bool IsTrue
        {
            get { return (Value as bool?) == true; }
        }

        public bool IsList
        {
            get { return Value is ArrayList; }
        }

        internal bool IsOfTypeInt
        {
            get { return Value is int?; }
        }

        public bool IsInt
        {
            get
            {
                int value;
                return Value != null && (Value is int || Int32.TryParse(Value.ToString(), out value));
            }
        }

        public int AsInt
        {
            get { return IsList ? 0 : Convert.ToInt32(Value); }
        }

        public bool IsString
        {
            get { return Value is string; }
        }

        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var v = (obj as ValueObject).Value;
            if (Value == null && v == null) return true;
            if (Value == null || v == null) return false;
            if (IsList || (obj as ValueObject).IsList)
                return Value.ToString().Equals(v.ToString());
            return Value.Equals(v);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            if (IsList)
            {
                var l = (from object v in AsList select v.ToString()).ToList();
                return string.Format("[{0}]", String.Join(", ", l));
            }
            return (Value ?? "").ToString();
        }

        internal void Add(ValueObject increment)
        {
            if (increment == null) throw new ArgumentNullException("increment");

            if (increment.Value == null) throw new InvalidOperationException("increment.Value is null");

            if (Value == null) throw new InvalidOperationException("Value is null");

            if (increment.IsOfTypeInt)
            {
                if (IsList)
                    (Value as ArrayList).Add(increment.AsInt);
                else
                    Value = increment.AsInt + AsInt;
            }
            else
            {
                var l = new ArrayList();
                if (IsList)
                {
                    l.AddRange(AsList);
                }
                else
                {
                    l.Add(Value);
                }
                if (increment.IsList)
                {
                    l.AddRange(increment.AsList);
                }
                else
                {
                    l.Add(increment);
                }
                Value = l;
            }
        }

        public ArrayList AsList
        {
            get { return IsList ? (Value as ArrayList) : (new ArrayList(new[] { Value })); }
        }
    }

    public class SingleMatchResult
    {
        public SingleMatchResult(int index, DiscordArgPattern match)
        {
            Position = index;
            Match = match;
        }

        public SingleMatchResult()
        {
        }

        public int Position { get; set; }
        public DiscordArgPattern Match { get; set; }
    }

    public class MatchResult
    {
        public bool Matched;
        public IList<DiscordArgPattern> Left;
        public IEnumerable<DiscordArgPattern> Collected;

        public MatchResult() { }

        public MatchResult(bool matched, IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected)
        {
            Matched = matched;
            Left = left;
            Collected = collected;
        }

        public bool LeftIsEmpty { get { return Left.Count == 0; } }

        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("matched={0} left=[{1}], collected=[{2}]",
                Matched,
                Left == null ? "" : string.Join(", ", Left.Select(p => p.ToString())),
                Collected == null ? "" : string.Join(", ", Collected.Select(p => p.ToString()))
            );
        }
    }

    public class DiscordArgTokens : IEnumerable<string>
    {
        private readonly Type _errorType;
        private readonly List<string> _tokens = new List<string>();

        public DiscordArgTokens(IEnumerable<string> source)
        {
            _tokens.AddRange(source);
        }

        public DiscordArgTokens(string source)
        {
            _tokens.AddRange(source.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
        }

        public Type ErrorType
        {
            get { return _errorType; }
        }

        public static DiscordArgTokens FromPattern(string pattern)
        {
            var spacedOut = Regex.Replace(pattern, @"([\[\]\(\)\|]|\.\.\.)", @" $1 ");
            var source = Regex.Split(spacedOut, @"\s+|(\S*<.*?>)").Where(x => !string.IsNullOrEmpty(x));
            return new DiscordArgTokens(source);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return _tokens.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string Move()
        {
            string s = null;
            if (_tokens.Count > 0)
            {
                s = _tokens[0];
                _tokens.RemoveAt(0);
            }
            return s;
        }

        public string Current()
        {
            return (_tokens.Count > 0) ? _tokens[0] : null;
        }

        public override string ToString()
        {
            return string.Format("current={0},count={1}", Current(), _tokens.Count);
        }
    }

    public class DiscordArgPattern
    {
        public ValueObject Value { get; set; }

        public virtual string Name
        {
            get { return ToString(); }
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ToString() == obj.ToString();
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public virtual bool HasChildren
        {
            get { return Children != null && Children.Count() > 0; }
        }

        public IList<DiscordArgPattern> Children { get; set; }

        public virtual MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            return new MatchResult();
        }
    }

    public class DiscordArgBranchPattern : DiscordArgPattern
    {

        public DiscordArgBranchPattern(params DiscordArgPattern[] children)
        {
            if (children == null) throw new ArgumentNullException("children");
            Children = children;
        }

        public override bool HasChildren { get { return true; } }

        /*public IEnumerable<Pattern> Flat<T>() where T : Pattern
        {
            return Flat(typeof(T));
        }

        public override ICollection<Pattern> Flat(params Type[] types)
        {
            if (types == null) throw new ArgumentNullException("types");
            if (types.Contains(this.GetType()))
            {
                return new Pattern[] { this };
            }
            return Children.SelectMany(child => child.Flat(types)).ToList();
        */

        public override string ToString()
        {
            return string.Format("{0}({1})", GetType().Name, String.Join(", ", Children.Select(c => c == null ? "None" : c.ToString())));
        }
    }

    public class DiscordArgLeafPattern : DiscordArgPattern
    {
        private readonly string _name;

        protected DiscordArgLeafPattern(string name, ValueObject value = null)
        {
            _name = name;
            Value = value;
        }

        protected DiscordArgLeafPattern()
        {
        }

        public override string Name
        {
            get { return _name; }
        }

        /*public override ICollection<Pattern> Flat(params Type[] types)
        {
            if (types == null) throw new ArgumentNullException("types");
            if (types.Length == 0 || types.Contains(this.GetType()))
            {
                return new Pattern[] { this };
            }
            return new Pattern[] { };
        }*/

        public virtual SingleMatchResult SingleMatch(IList<DiscordArgPattern> patterns)
        {
            return new SingleMatchResult();
        }

        public override MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            var coll = collected ?? new List<DiscordArgPattern>();
            var sresult = SingleMatch(left);
            var match = sresult.Match;
            if (match == null)
            {
                return new MatchResult(false, left, coll);
            }
            var left_ = new List<DiscordArgPattern>();
            left_.AddRange(left.Take(sresult.Position));
            left_.AddRange(left.Skip(sresult.Position + 1));
            var sameName = coll.Where(a => a.Name == Name).ToList();
            if (Value != null && (Value.IsList || Value.IsOfTypeInt))
            {
                var increment = new ValueObject(1);
                if (!Value.IsOfTypeInt)
                {
                    increment = match.Value.IsString ? new ValueObject(new[] { match.Value }) : match.Value;
                }
                if (sameName.Count == 0)
                {
                    match.Value = increment;
                    var res = new List<DiscordArgPattern>(coll) { match };
                    return new MatchResult(true, left_, res);
                }
                sameName[0].Value.Add(increment);
                return new MatchResult(true, left_, coll);
            }
            var resColl = new List<DiscordArgPattern>();
            resColl.AddRange(coll);
            resColl.Add(match);
            return new MatchResult(true, left_, resColl);
        }

        public override string ToString()
        {
            return string.Format("{0}({1}, {2})", GetType().Name, Name, Value);
        }
    }

    public class DiscordArgRequired : DiscordArgBranchPattern
    {
        public DiscordArgRequired(params DiscordArgPattern[] patterns)
            : base(patterns)
        {
        }

        public override MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            var coll = collected ?? new List<DiscordArgPattern>();
            var l = left;
            var c = coll;
            foreach (var pattern in Children)
            {
                var res = pattern.Match(l, c);
                l = res.Left;
                c = res.Collected;
                if (!res.Matched)
                    return new MatchResult(false, left, coll);
            }
            return new MatchResult(true, l, c);
        }
    }

    public class DiscordArgEither : DiscordArgBranchPattern
    {
        public DiscordArgEither(params DiscordArgPattern[] patterns) : base(patterns)
        {
        }

        public override MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            var coll = collected ?? new List<DiscordArgPattern>();
            var outcomes =
                Children.Select(pattern => pattern.Match(left, coll))
                        .Where(outcome => outcome.Matched)
                        .ToList();
            if (outcomes.Count != 0)
            {
                var minCount = outcomes.Min(x => x.Left.Count);
                return outcomes.First(x => x.Left.Count == minCount);
            }
            return new MatchResult(false, left, coll);
        }
    }

    public class DiscordArgOneOrMore : DiscordArgBranchPattern
    {
        public DiscordArgOneOrMore(params DiscordArgPattern[] patterns)
            : base(patterns)
        {
        }

        public override MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            //Debug.Assert(Children.Count == 1);
            var coll = collected ?? new List<DiscordArgPattern>();
            var l = left;
            var c = coll;
            IList<DiscordArgPattern> l_ = null;
            var matched = true;
            var times = 0;
            while (matched)
            {
                // could it be that something didn't match but changed l or c?
                var res = Children[0].Match(l, c);
                matched = res.Matched;
                l = res.Left;
                c = res.Collected;
                times += matched ? 1 : 0;
                if (l_ != null && l_.Equals(l))
                    break;
                l_ = l;
            }
            if (times >= 1)
            {
                return new MatchResult(true, l, c);
            }
            return new MatchResult(false, left, coll);
        }
    }

    public class DiscordArgOptional : DiscordArgBranchPattern
    {
        public DiscordArgOptional(params DiscordArgPattern[] patterns) : base(patterns)
        {

        }

        public override MatchResult Match(IList<DiscordArgPattern> left, IEnumerable<DiscordArgPattern> collected = null)
        {
            var c = collected ?? new List<DiscordArgPattern>();
            var l = left;
            foreach (var pattern in Children)
            {
                var res = pattern.Match(l, c);
                l = res.Left;
                c = res.Collected;
            }
            return new MatchResult(true, l, c);
        }
    }

    public class DiscordArgArgument : DiscordArgLeafPattern
    {
        public DiscordArgArgument(string name, ValueObject value = null) : base(name, value)
        {
        }

        public DiscordArgArgument(string name, string value)
            : base(name, new ValueObject(value))
        {
        }

        public DiscordArgArgument(string name, ICollection coll)
            : base(name, new ValueObject(coll))
        {
        }

        public DiscordArgArgument(string name, int value)
            : base(name, new ValueObject(value))
        {
        }

        public override SingleMatchResult SingleMatch(IList<DiscordArgPattern> left)
        {
            for (var i = 0; i < left.Count; i++)
            {
                if (left[i] is DiscordArgArgument)
                    return new SingleMatchResult(i, new DiscordArgArgument(Name, left[i].Value));
            }
            return new SingleMatchResult();
        }

        /*public override Node ToNode()
        {
            return new ArgumentNode(this.Name, (this.Value != null && this.Value.IsList) ? ValueType.List : ValueType.String);
        }

        public override string GenerateCode()
        {
            var s = Name.Replace("<", "").Replace(">", " ").ToLowerInvariant();
            s = "Arg" + GenerateCodeHelper.ConvertDashesToCamelCase(s);

            if (Value != null && Value.IsList)
            {
                return string.Format("public ArrayList {0} {{ get {{ return _args[\"{1}\"].AsList; }} }}", s, Name);
            }
            return string.Format("public string {0} {{ get {{ return _args[\"{1}\"].ToString(); }} }}", s, Name);
        }*/
    }

    public class DiscordArgCommand : DiscordArgArgument
    {
        public DiscordArgCommand(string name, ValueObject value = null) : base(name, value ?? new ValueObject(false))
        {
        }

        public override SingleMatchResult SingleMatch(IList<DiscordArgPattern> left)
        {
            for (var i = 0; i < left.Count; i++)
            {
                var pattern = left[i];
                if (pattern is DiscordArgArgument)
                {
                    if (pattern.Value.ToString() == Name)
                        return new SingleMatchResult(i, new DiscordArgCommand(Name, new ValueObject(true)));
                    break;
                }
            }
            return new SingleMatchResult();
        }

        /*public override Node ToNode() { return new CommandNode(this.Name); }

        public override string GenerateCode()
        {
            var s = Name.ToLowerInvariant();
            s = "Cmd" + GenerateCodeHelper.ConvertDashesToCamelCase(s);
            return string.Format("public bool {0} {{ get {{ return _args[\"{1}\"].IsTrue; }} }}", s, Name);
        }*/

    }

    public class DiscordCommandLineArgs
    {
        public string Command
        {
            get { return m_command; }
        }

        public string Pattern
        {
            get { return m_pattern; }
        }

        public string Help
        {
            get { return m_help; }
        }

        private string m_command;
        private string m_pattern;
        private string m_help;

        private DiscordArgRequired m_requiredPattern;

        public DiscordCommandLineArgs(string command, string pattern, string help)
        {
            m_command   = command;
            m_pattern   = pattern;
            m_help      = help;

            Parse(m_pattern);
        }

        public ListDictionary Match(string text)
        {
            string[] args = text.Split(new char[] { ' ' });
            return Match(args);
        }

        public ListDictionary Match(string[] argV)
        {
            var arguments = ParseArgv(argV);
            var res = m_requiredPattern.Match(arguments);
            if (res.Matched && res.LeftIsEmpty)
            {
                var dict = new ListDictionary();
                /*foreach (var p in pattern.Flat())
                {
                    dict[p.Name] = p.Value;
                }*/ 
                foreach (var p in res.Collected)
                {
                    dict[p.Name] = p.Value;
                }
                return dict;
            }

            return null;
        }

        public bool IsMatch(string text)
        {
            string[] args = text.Split(new char[] { ' ' });
            return IsMatch(args);
        }

        public bool IsMatch(string[] args)
        {
            var arguments = ParseArgv(args);
            var res = m_requiredPattern.Match(arguments);
            if (res.Matched && res.LeftIsEmpty)
            {
                return true;
            }

            return false;

        }

        private bool Parse(string pattern)
        {
            string fullPattern = "!" + m_command + " " + m_pattern;
            DiscordArgTokens tokenParser = DiscordArgTokens.FromPattern(fullPattern);
            IEnumerable<DiscordArgPattern> patterns = ParseExpression(tokenParser);
            m_requiredPattern = new DiscordArgRequired(patterns.ToArray());
            return true;
        }

        private IEnumerable<DiscordArgPattern> ParseExpression(DiscordArgTokens tokenParser)
        {
            var seq = ParseSequence(tokenParser);
            if (tokenParser.Current() != "|")
                return seq;
            var result = new List<DiscordArgPattern>();
            if (seq.Count() > 1)
            {
                result.Add(new DiscordArgRequired(seq.ToArray()));
            }
            else
            {
                result.AddRange(seq);
            }
            while (tokenParser.Current() == "|")
            {
                tokenParser.Move();
                seq = ParseSequence(tokenParser);
                if (seq.Count() > 1)
                {
                    result.Add(new DiscordArgRequired(seq.ToArray()));
                }
                else
                {
                    result.AddRange(seq);
                }
            }
            result = result.Distinct().ToList();
            if (result.Count > 1)
                return new[] { new DiscordArgEither(result.ToArray()) };
            return result;
        }

        private IEnumerable<DiscordArgPattern> ParseSequence(DiscordArgTokens tokenParser)
        {
            var result = new List<DiscordArgPattern>();
            while (!new[] { null, "]", ")", "|" }.Contains(tokenParser.Current()))
            {
                var atom = ParseAtom(tokenParser);
                if (tokenParser.Current() == "...")
                {
                    result.Add(new DiscordArgOneOrMore(atom.ToArray()));
                    tokenParser.Move();
                    return result;
                }
                result.AddRange(atom);
            }
            return result;
        }

        private IEnumerable<DiscordArgPattern> ParseAtom(DiscordArgTokens tokenParser)
        {
            // atom ::= '(' expr ')' | '[' expr ']' | 'options'
            //  | long | shorts | argument | command ;            

            var token = tokenParser.Current();
            var result = new List<DiscordArgPattern>();
            switch (token)
            {
                case "[":
                case "(":
                    {
                        tokenParser.Move();
                        string matching;
                        if (token == "(")
                        {
                            matching = ")";
                            result.Add(new DiscordArgRequired(ParseExpression(tokenParser).ToArray()));
                        }
                        else
                        {
                            matching = "]";
                            result.Add(new DiscordArgOptional(ParseExpression(tokenParser).ToArray()));
                        }
                        if (tokenParser.Move() != matching)
                            throw new Exception("unmatched '" + token + "'");
                    }
                    break;
                default:
                    if ((token.StartsWith("<") && token.EndsWith(">")) || token.All(c => Char.IsUpper(c)))
                    {
                        result.Add(new DiscordArgArgument(tokenParser.Move()));
                    }
                    else
                    {
                        result.Add(new DiscordArgCommand(tokenParser.Move()));
                    }
                    break;
            }
            return result;
        }

        private static IList<DiscordArgPattern> ParseArgv(string[] arguments)
        {
            //    If options_first:
            //        argv ::= [ long | shorts ]* [ argument ]* [ '--' [ argument ]* ] ;
            //    else:
            //        argv ::= [ long | shorts | argument ]* [ '--' [ argument ]* ] ;

            var parsed = new List<DiscordArgPattern>();
            foreach(string arg in arguments)
            { 
                parsed.Add(new DiscordArgArgument(null, new ValueObject(arg)));
            }
            return parsed;
        }
    }
}
