﻿using D1Droid;
using System;

namespace DiscordDomain.Helpers
{
    public enum ProxyMessageType
    {
        InitaliseDomain,
        RestartComplete,
        ScriptDomainStarted,

        RequestUnloadDomain,
        UnloadDomain,

        RequestLoadAllServers,
        RequestLoadServer,
        RequestUnloadServer,
        UnloadedServer,

        FailedShuttingDown,
        FailedUnloadServer,
        FailedUnloading
    }

    public interface IDiscordDomainEventProxy
    {
        ProxyMessageType ProxyMessage { get; }
    }

    public class DiscordDomainEvent : MarshalByRefObject, IDiscordDomainEventProxy
    {
        
        public ProxyMessageType ProxyMessage
        {
            get
            {
                return m_proxyMessage;
            }
        }

        public string Message
        {
            get
            {
                return m_message;
            }
        }

        private ProxyMessageType m_proxyMessage;
        private string m_message;

        public DiscordDomainEvent(ProxyMessageType proxyMessage)
        {
            m_proxyMessage = proxyMessage;
        }

        public DiscordDomainEvent(ProxyMessageType proxyMessage, string message)
        {
            m_proxyMessage = proxyMessage;
            m_message = message;
        }
    }

    public class DiscordInitaliseDomainEvent : MarshalByRefObject, IDiscordDomainEventProxy
    {
        public CommandLineArgs CommandLineArgs
        {
            get { return m_commandLineArgs;  }
        }

        public DateTime AppStartTime
        {
            get { return m_creationTime; }
        }

        public ProxyMessageType ProxyMessage
        {
            get { return m_proxyMessage; }
        }

        private CommandLineArgs m_commandLineArgs;
        private ProxyMessageType m_proxyMessage;
        private DateTime m_creationTime;

        public DiscordInitaliseDomainEvent(CommandLineArgs args, DateTime appStartTime)
        {
            m_proxyMessage      = ProxyMessageType.InitaliseDomain;
            m_commandLineArgs   = args;
            m_creationTime      = appStartTime;
        }
    }

    public class DiscordServerDomainEvent : MarshalByRefObject, IDiscordDomainEventProxy
    {
        public ProxyMessageType ProxyMessage
        {
            get
            {
                return m_proxyMessage;
            }
        }

        public string ServerName
        {
            get { return m_serverName; }
        }

        public string ServerId
        {
            get { return m_serverId; }
        }

        private ProxyMessageType m_proxyMessage;
        private string m_serverName;
        private string m_serverId;

        public DiscordServerDomainEvent(string serverName, string serverId, bool unloaded)
        {
            m_proxyMessage  = unloaded ? ProxyMessageType.UnloadedServer : ProxyMessageType.RequestLoadServer;
            m_serverName    = serverName;
            m_serverId      = serverId;
        }
    }
}
