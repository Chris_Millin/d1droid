﻿using D1DroidEngine.Helper;
using DiscordSharp;
using DiscordSharp.Events;
using System;
using System.Collections.Specialized;

namespace DiscordDomain.Helpers
{
    public interface IDiscordRule
    {
        bool IsEventSupported(DiscordEventType eventType);

        bool Validate(EventArgs e);

        ListDictionary BuildParameters(EventArgs e);
    }

    public class DiscordBasicRule : IDiscordRule
    {
        public bool IsEventSupported(DiscordEventType eventType)
        {
            if(eventType != DiscordEventType.MessageRecieved || eventType != DiscordEventType.PrivateMessageRecieved)
            {
                return true;
            }

            return false;
        }

        public ListDictionary BuildParameters(EventArgs e)
        {
            return new ListDictionary();
        }

        public bool Validate(EventArgs e)
        {
            return true;
        }
    }

    public class DiscordMessageRule : IDiscordRule
    { 
        public string Command
        {
            get { return m_triggerCommand; }
            set { m_triggerCommand = value; }
        }

        public string Pattern
        {
            get { return m_pattern; }
        }

        public string Help
        {
            get { return m_help; }
        }

        private string m_triggerCommand;
        private string m_pattern    = string.Empty;
        private string m_help       = string.Empty;
        private DiscordCommandLineArgs m_arguments;


        public DiscordMessageRule(string command, string help)
        {
            m_triggerCommand    = command;
            m_help              = help;
        }

        public DiscordMessageRule(string command, string pattern, string help)
        {
            m_triggerCommand    = command;
            m_pattern           = pattern;
            m_help              = help;
            m_arguments = new DiscordCommandLineArgs(command, pattern, help);
        }

        public bool IsEventSupported(DiscordEventType eventType)
        {
            if (eventType == DiscordEventType.MessageRecieved ||
                eventType == DiscordEventType.PrivateMessageRecieved)
            {
                return true;
            }

            return false;
        }

        public bool Validate(EventArgs e)
        {
            bool valid = false;
            if (e.GetType() == typeof(DiscordMessageEventArgs))
            {
                valid = ValidateMessage((DiscordMessageEventArgs)e);
            }
            else if(e.GetType() == typeof(DiscordPrivateMessageEventArgs))
            {
                valid = ValidatePrivateMessage((DiscordPrivateMessageEventArgs)e);
            }
            else
            {
                valid = false;
            }

            return valid;
        }

        public bool ValidateMessage(DiscordMessageEventArgs e)
        {
            string content = e.Message.Content;
            return ValidateContent(content);
        }

        public bool ValidatePrivateMessage(DiscordPrivateMessageEventArgs e)
        {
            string content = e.Message;
            return ValidateContent(content);
        }

        private bool ValidateContent(string content)
        {
            bool valid = false;
            try
            {
                
                if(m_arguments != null)
                {
                    return m_arguments.IsMatch(content);
                }
                else
                {
                    string paddedContent = content + " "; //add padding when arguments arent specified
                    return paddedContent.StartsWith("!" + Command + " ");
                }
            }
            catch
            {
                valid = false;
            }

            return valid;
        }

        public ListDictionary BuildParameters(EventArgs e)
        {
            if (e.GetType() == typeof(DiscordMessageEventArgs) && m_arguments != null)
            {
                return m_arguments.Match(((DiscordMessageEventArgs)e).MessageText);
            }
            else if (e.GetType() == typeof(DiscordPrivateMessageEventArgs) && m_arguments != null)
            {
                return m_arguments.Match(((DiscordPrivateMessageEventArgs)e).Message);
            }
            else
            {
                return new ListDictionary();
            }
        }
    }
}
