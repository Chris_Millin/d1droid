﻿namespace DiscordDomain.Helpers
{
    public interface IDiscordDomainHandlerProxy
    {
        void ProxyMessage(IDiscordDomainHandlerProxy sender, IDiscordDomainEventProxy msg);
    }
}
