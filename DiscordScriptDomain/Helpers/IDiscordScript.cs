﻿using System;

namespace DiscordDomain.Helpers
{
    public interface IDiscordScript : IDisposable
    {
        DiscordScriptClient ActiveClient
        {
            get;
            set;
        }

        /// <summary>
        /// When the script is being compiled
        /// </summary>
        void RegisterCommands();
    }
}
