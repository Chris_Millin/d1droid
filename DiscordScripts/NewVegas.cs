using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Collections.Specialized;

//ASM:Newtonsoft.Json.dll
namespace DiscordDomain.CSharp
{
    public class NewVegas : DiscordScript
    {
        private struct level
        {
            public int hourMin;
            public int hourMax;
            public string levelText;
        }

        List<level> m_level = new List<level>();

        public NewVegas()
        {
            AddLevel(1600, 1630, "Level: Fast Travel Cynic");
            AddLevel(1631, 1650, "Level: Mojave Turtle");
            AddLevel(1651, 1670, "Level: Certified NPC");
            AddLevel(1671, 1690, "Level: Second Life Surrogate"); 
            AddLevel(1691, 1710, "Level: Wasteland Junky");
        }

        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("nv", "Shows Adams New vegas starts", MessageRecieved);
        }

        public override void Dispose()
        {
            //throw new NotImplementedException();
        }

        public void MessageRecieved(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            string hours = GetHoursPlayed();
            string achievement = GetLevel(hours);

            string message = string.Format("Adam has played Fallout: New Vegas for {0} hours {1}", hours, achievement);

            e.Channel.SendMessage(message);
        }


        public string GetHoursPlayed()
        {
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString("http://steamcommunity.com/id/mikemareen/games/?tab=all&sort=name");
                foreach (string line in ReadLines(htmlCode))
                {
                    if (line.StartsWith("var rgGames = "))
                    {
                        string json = string.Empty;
                        json = line.Replace("var rgGames = ", "{'games':");
                        if(json.EndsWith(";"))
                        {
                            json = json.Replace(';', '}');
                        }

                        dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                        foreach(var item in obj.games)
                        {
                            if(item.name == "Fallout: New Vegas")
                            {
                                return item.hours_forever;
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetLevel(string hours)
        {
            hours = hours.Replace(",", "");
            int hoursInt = 0;
            if (int.TryParse(hours, out hoursInt))
            {
                foreach(level lv in m_level)
                {
                    if(hoursInt < lv.hourMax && hoursInt >= lv.hourMin)
                    {
                        return lv.levelText;
                    }
                }
            }

            return string.Empty;
        }

        private void AddLevel(int min, int max, string level)
        {
            level lv = new NewVegas.level();
            lv.hourMin = min;
            lv.hourMax = max;
            lv.levelText = level;
            m_level.Add(lv);
        }

        private IEnumerable<string> ReadLines(string text)
        {
            string line = string.Empty;
            using (StringReader reader = new StringReader(text))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line.Trim();
                }
            }
        }

        
    }
}
