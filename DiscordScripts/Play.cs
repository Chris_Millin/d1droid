using D1DroidEngine.Helper;
using DiscordDomain.Admin;
using DiscordEngine.Helpers;
using DiscordSharp;
using DiscordSharp.Events;
using DiscordSharp.Objects;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using VideoLibrary;

//ASM:libvideo.dll,NAudio.Vorbis.dll,NAudio.dll
namespace DiscordDomain.CSharp
{
    public class Play : DiscordScript
    {
        public enum PlayType
        {
            Single,
            Group,
            Amount,
            Length
        }



        public struct PlayInfo
        {
            public string clip;
            public DiscordChannel channel;
        }

        public struct PlayCD
        {
            public DateTime lastCMD;
            public int amount;
        }

        public class UserCD
        {
            public PlayCD[] CoolDown = new PlayCD[(int)PlayType.Length];
        }

        public class ClipInfo
        {
            public string File;
            public int PlayCount;
            public string AddedBy;

            public ClipInfo(string file, int count, string addby)
            {
                File = file;
                PlayCount = count;
                AddedBy = addby;
            }
        }

        public Dictionary<string, ClipInfo> m_clips = new Dictionary<string, ClipInfo>();
        public Dictionary<string, string> m_intros = new Dictionary<string, string>();
        public Queue<PlayInfo> m_clipQueue = new Queue<PlayInfo>();
        public object m_mutex = new object();

        public const string c_clipFolder = "audio_clips";
        public const string c_clipStore = "play_clip_dictionary.xml";
        public const string c_introStore = "play_intro_dictionary.xml";

        public DiscordChannel m_currentChannel;
        public bool m_consume = true;

        public Thread m_consumerThread;
        private AutoResetEvent stopConsumer = new AutoResetEvent(false);

        public Dictionary<string, DiscordMember> m_currentConnected = new Dictionary<string, DiscordMember>();

        public Dictionary<string, UserCD> m_coolDown = new Dictionary<string, UserCD>();

        public Play()
        {

        }

        public override void RegisterCommands()
        {
            Initalise();
            ActiveClient.RegisterCommand("play", "list", "list all the commands", List);
            ActiveClient.RegisterCommand("play", "stop", "stop whats playing", Stop);
            ActiveClient.RegisterCommand("play", "<name> <amount>", "!play [(add <clip>) | remove | (update <clip>) | rename <oldname> ] <name> [<amount>]", PlayClip);
            ActiveClient.RegisterCommand("play", "[(add <clip>) | remove | (update <clip>) | (rename <oldname>) ] <name>", "!play [(add <clip>) | remove | (update <clip>) | rename <oldname> ] <name> [<amount>]", PlayClip);
            ActiveClient.RegisterCommand("play", "intro (set <clip>| remove)", "!play intro (set <clip>| remove)", Intro);
            ActiveClient.RegisterEvent<DiscordVoiceStateUpdateEventArgs>(DiscordEventType.VoiceStateUpdated, CheckIntro);
        }

        public void Initalise()
        {
            string dataFolder = Path.Combine(ActiveClient.DataLocation, c_clipFolder);
            if (!Directory.Exists(dataFolder))
            {
                Directory.CreateDirectory(dataFolder);
            }

            LoadExistingClips();
            LoadExistingIntros();
            m_consumerThread = new Thread(ConsumerThread);
            m_consumerThread.Start();
        }

        public void LoadExistingClips()
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                foreach (XElement clip in clipStoreXml.Root.Elements())
                {
                    string name = string.Empty;
                    string tag = string.Empty;
                    int playCount = 0;
                    string addedBy = ActiveClient.Client.Me.Username;
                    if (clip.Attribute("name") != null)
                        name = clip.Attribute("name").Value;
                    if (clip.Attribute("tag") != null)
                        tag = clip.Attribute("tag").Value;
                    if (clip.Attribute("count") != null)
                        playCount = int.Parse(clip.Attribute("count").Value);
                    if (clip.Attribute("add") != null)
                        addedBy = clip.Attribute("add").Value;

                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(tag))
                    {
                        try
                        {
                            ClipInfo info = new ClipInfo(name, playCount, addedBy);
                            m_clips.Add(tag.ToLower(), info);
                        }
                        catch
                        {
                            using (DiscordDebugLogger logger = new DiscordDebugLogger())
                            {
                                Logging.LogError(LogType.Client, "Cant add clip " + name + " as tag " + tag + " has already been registered");
                            }
                        }
                    }
                }
            }
            else
            {
                clipStoreXml = new XDocument();
                XElement root = new XElement("AudioClipStore");
                clipStoreXml.Add(root);
                clipStoreXml.Save(clipStore);
            }
        }

        public void LoadExistingIntros()
        {
            XDocument introStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_introStore);
            if (File.Exists(clipStore))
            {
                introStoreXml = XDocument.Load(clipStore);
                foreach (XElement clip in introStoreXml.Root.Elements())
                {
                    string name = string.Empty;
                    string user = string.Empty;
                    if (clip.Attribute("name") != null)
                        name = clip.Attribute("name").Value;
                    if (clip.Attribute("user") != null)
                        user = clip.Attribute("user").Value;

                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(user))
                    {
                        try
                        {
                            m_intros.Add(user, name);
                        }
                        catch
                        {
                            using (DiscordDebugLogger logger = new DiscordDebugLogger())
                            {
                                Logging.LogError(LogType.Client, "Cant add clip " + name + " as user " + user + " has already been registered");
                            }
                        }
                    }
                }
            }
            else
            {
                introStoreXml = new XDocument();
                XElement root = new XElement("AudioIntroStore");
                introStoreXml.Add(root);
                introStoreXml.Save(clipStore);
            }
        }

        public void AddClip(string file, string triggerName, string userAdded)
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                XElement clip = new XElement("AudioClip");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("tag", triggerName));
                clip.Add(new XAttribute("count", 0));
                clip.Add(new XAttribute("add", userAdded));
                clipStoreXml.Root.Add(clip);
                clipStoreXml.Save(clipStore);
            }
            else
            {
                clipStoreXml = new XDocument();
                XElement root = new XElement("AudioClipStore");
                XElement clip = new XElement("AudioClip");
                clip.Add(new XAttribute("name", file));
                clip.Add(new XAttribute("tag", triggerName));
                clip.Add(new XAttribute("count", 0));
                clip.Add(new XAttribute("add", userAdded));
                root.Add(clip);
                clipStoreXml.Add(root);
                clipStoreXml.Save(clipStore);
            }
        }

        public void AddIntro(string file, string userID)
        {
            XDocument introStoreXml;
            string introStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_introStore);
            if (File.Exists(introStore))
            {
                introStoreXml = XDocument.Load(introStore);
                XElement intro = new XElement("AudioIntro");
                intro.Add(new XAttribute("name", file));
                intro.Add(new XAttribute("user", userID));
                introStoreXml.Root.Add(intro);
                introStoreXml.Save(introStore);
            }
            else
            {
                introStoreXml = new XDocument();
                XElement root = new XElement("AudioIntroStore");
                XElement intro = new XElement("AudioIntro");
                intro.Add(new XAttribute("name", file));
                intro.Add(new XAttribute("user", userID));
                root.Add(intro);
                introStoreXml.Add(root);
                introStoreXml.Save(introStore);
            }
        }

        public void RemoveClip(string triggerName)
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                XElement element = clipStoreXml.Root.Elements().FirstOrDefault(e =>
                {
                    string name = string.Empty;
                    if (e.Attribute("tag") != null)
                        name = e.Attribute("tag").Value;
                    if (name == triggerName)
                        return true;
                    else
                        return false;
                });
                if (element != null)
                {
                    element.Remove();
                }

                clipStoreXml.Save(clipStore);
            }
        }

        public void RemoveIntro(string userID)
        {
            XDocument introStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_introStore);
            if (File.Exists(clipStore))
            {
                introStoreXml = XDocument.Load(clipStore);
                XElement element = introStoreXml.Root.Elements().FirstOrDefault(e =>
                {
                    string name = string.Empty;
                    if (e.Attribute("user") != null)
                        name = e.Attribute("user").Value;
                    if (name == userID)
                        return true;
                    else
                        return false;
                });
                if (element != null)
                {
                    element.Remove();
                }

                introStoreXml.Save(clipStore);
            }
        }

        public void UpdateClipPlay(string tag, ClipInfo info)
        {
            XDocument clipStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_clipStore);
            if (File.Exists(clipStore))
            {
                clipStoreXml = XDocument.Load(clipStore);
                XElement element = clipStoreXml.Root.Elements().FirstOrDefault(e =>
                {
                    string name = string.Empty;
                    if (e.Attribute("tag") != null)
                        name = e.Attribute("tag").Value;
                    if (name == tag)
                        return true;
                    else
                        return false;
                });

                if (element.Attribute("count") != null)
                    element.Attribute("count").Value = info.PlayCount.ToString();
                else
                    element.Add(new XAttribute("count", info.PlayCount));

                if (element.Attribute("add") == null)
                    element.Add(new XAttribute("add", info.AddedBy));

                clipStoreXml.Save(clipStore);
            }
        }

        public void Stop(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            DiscordVoiceClient vc = ActiveClient.Client.GetVoiceClient(ActiveClient.Server);
            if (vc != null)
                ActiveClient.Client.DisconnectFromVoice(vc);

            lock (m_mutex)
            {
                m_clipQueue.Clear();
            }
        }

        public void List(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            StringBuilder sb = new StringBuilder(1400, 1400);

            List<string> clips = m_clips.Keys.ToList();
            clips.Sort();
            int count = 0;
            int printedCount = 0;
            e.Author.SendMessage("```Total Clips: " + clips.Count + "```");
            sb.AppendLine("`Audio Clip Triggers:\n`");
            while (printedCount != clips.Count())
            {
                count = 0;
                foreach (var name in clips.Skip(printedCount))
                {
                    try
                    {
                        string toAdd = "Clip: `" + name + "` Played: `" + m_clips[name].PlayCount + "` Added By: `" + m_clips[name].AddedBy + "`";
                        count++;
                        if (toAdd.Length + sb.Length > sb.MaxCapacity)
                        {
                            e.Author.SendMessage(sb.ToString());
                            Thread.Sleep(1000);
                            sb = new StringBuilder(1400, 1400);
                            sb.AppendLine(toAdd);
                            break;
                        }
                        else
                        {
                            sb.AppendLine(toAdd);
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        e.Author.SendMessage(sb.ToString());
                        Thread.Sleep(1000);
                        sb = new StringBuilder(1400, 1400);
                        break;
                    }
                }

                printedCount += count;
            }

            if (count != 0)
                e.Author.SendMessage(sb.ToString());
        }

        public void PlayClip(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            string name = string.Empty;
            if (!parameters.Contains("<name>"))
            {
                e.Channel.SendMessage("Err: argument <name> is missing cant add/update/remove or play the clip");
                return;
            }
            else
            {
                ValueObject param = (ValueObject)parameters["<name>"];
                name = (string)param.Value;
                name = name.ToLower();
            }

            if (parameters.Contains("add"))
            {
                if (m_clips.ContainsKey(name))
                {
                    e.Channel.SendMessage("Err: name " + name + " is already in uses");
                    return;
                }

                string clip = string.Empty;
                if (parameters.Contains("<clip>"))
                {
                    ValueObject param = (ValueObject)parameters["<clip>"];
                    clip = (string)param.Value;
                }

                if (string.IsNullOrEmpty(clip))
                {
                    e.Channel.SendMessage("Err: Cant add empty clip");
                    return;
                }

                if (!clip.EndsWith(".mp3"))
                {
                    e.Channel.SendMessage("Err: Cant add fine as its not in .mp3 format " + clip);
                    return;
                }
                else
                {
                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            string saveLocation = Path.Combine(ActiveClient.DataLocation, c_clipFolder, name + ".mp3");
                            string saveShortLocation = Path.Combine(ActiveClient.DataShortLocation, c_clipFolder, name + ".mp3");
                            client.DownloadFile(clip, saveLocation);
                            AddClip(saveShortLocation, name, e.Author.Username);
                            m_clips.Add(name, new ClipInfo(saveShortLocation, 0, e.Author.Username));
                        }
                    }
                    catch (Exception ex)
                    {
                        e.Channel.SendMessage("Err: Something went wrong while trying to add " + name + ". Admin Info: `" + ex.Message + "`");
                    }
                }

                e.Channel.SendMessage("Added Clip " + name);
            }

            else if (parameters.Contains("remove"))
            {
                if (m_clips.ContainsKey(name))
                {
                    try
                    {
                        m_clips.Remove(name);
                        RemoveClip(name);
                        e.Channel.SendMessage("Removed Clip " + name);
                    }
                    catch (Exception ex)
                    {
                        e.Channel.SendMessage("Err: Something went wrong while trying to remove " + name + ". Admin Info: `" + ex.Message + "`");
                    }
                }
                else
                {
                    e.Channel.SendMessage("No Clip " + name + " to remove");
                }
            }
            else if (parameters.Contains("update"))
            {
                if (m_clips.ContainsKey(name))
                {
                    try
                    {
                        string clip = string.Empty;
                        if (parameters.Contains("<clip>"))
                        {
                            ValueObject param = (ValueObject)parameters["<clip>"];
                            clip = (string)param.Value;
                        }

                        if (string.IsNullOrEmpty(clip))
                        {
                            e.Channel.SendMessage("Err: Cant add empty clip");
                            return;
                        }

                        m_clips.Remove(name);
                        RemoveClip(name);


                        using (WebClient client = new WebClient())
                        {
                            string saveLocation = Path.Combine(ActiveClient.DataLocation, c_clipFolder, name + ".mp3");
                            if (File.Exists(saveLocation))
                            {
                                File.Delete(saveLocation);
                            }
                            Thread.Sleep(1000);
                            string saveShortLocation = Path.Combine(ActiveClient.DataShortLocation, c_clipFolder, name + ".mp3");
                            client.DownloadFile(clip, saveLocation);
                            AddClip(saveShortLocation, name, e.Author.Username);
                            m_clips.Add(name, new ClipInfo(saveShortLocation, 0, e.Author.Username));
                        }

                        e.Channel.SendMessage("Updated Clip " + name);
                    }
                    catch (Exception ex)
                    {
                        e.Channel.SendMessage("Err: Something went wrong while trying to update " + name + ". Admin Info: `" + ex.Message + "`");
                    }

                }
                else
                {
                    e.Channel.SendMessage("No Clip " + name + " to remove");
                }
            }
            else if (parameters.Contains("rename"))
            {
                string oldname = string.Empty;
                if (parameters.Contains("<oldname>"))
                {
                    ValueObject param = (ValueObject)parameters["<oldname>"];
                    oldname = (string)param.Value;
                }

                if (string.IsNullOrEmpty(oldname))
                {
                    e.Channel.SendMessage("Err: Cant add empty old name");
                    return;
                }

                try
                {
                    if (m_clips.ContainsKey(oldname) && !m_clips.ContainsKey(name))
                    {
                        string clipFile = m_clips[oldname].File;
                        string saveLocation = Path.Combine(ActiveClient.DataLocation, c_clipFolder, name + ".mp3");
                        string saveShortLocation = Path.Combine(ActiveClient.DataShortLocation, c_clipFolder, name + ".mp3");
                        if (File.Exists(saveLocation))
                        {
                            e.Channel.SendMessage("Err: file with old name " + oldname + " already exists");
                            return;
                        }

                        m_clips.Remove(oldname);
                        RemoveClip(oldname);

                        File.Move(clipFile, saveLocation);

                        AddClip(saveShortLocation, name, m_clips[oldname].AddedBy);
                        m_clips.Add(name, new ClipInfo(saveShortLocation, m_clips[oldname].PlayCount, e.Author.Username));
                    }
                    else if (!m_clips.ContainsKey(oldname))
                    {
                        e.Channel.SendMessage("Err: no clip found with the old name");
                    }
                    else if (m_clips.ContainsKey(name))
                    {
                        e.Channel.SendMessage("Err: clip found with the new name");
                    }
                }
                catch (Exception ex)
                {
                    e.Channel.SendMessage("Err: Something went wrong while trying to rename " + name + ". Admin Info: `" + ex.Message + "`");
                }

            }
            else
            {
                int maxCount = 10;

                if (ActiveClient.Admin != null && e.Author.ID == ActiveClient.Admin.ID)
                    maxCount = int.MaxValue;

                PlayType type = PlayType.Single;

                List<string> plays;
                if (name.Contains(","))
                {
                    type = PlayType.Group;
                    plays = new List<string>(name.Split(new char[] { ',' }));
                    if (plays.Count > maxCount)
                        plays.RemoveRange(maxCount - 1, plays.Count - (maxCount - 1));
                }
                else
                {

                    int count = 1;
                    if (parameters.Contains("<amount>"))
                    {
                        ValueObject param = (ValueObject)parameters["<amount>"];
                        if (param.IsInt)
                        {
                            type = PlayType.Amount;
                            int amount = param.AsInt;
                            if (amount <= maxCount && amount >= 1)
                                count = amount;
                        }
                    }

                    plays = new List<string>();
                    for (int i = 0; i < count; i++)
                    {
                        string addName = name;
                        if (addName == "meme")
                        {
                            Random rand = new Random();
                            Thread.Sleep(100);
                            int id = rand.Next(0, m_clips.Count);
                            addName = m_clips.Keys.ElementAt(id);
                        }

                        plays.Add(addName);
                    }
                }

                if (!CanUserPlay(e.Author, type))
                {
                    TimeSpan span = DateTime.Now - m_coolDown[e.Author.ID].CoolDown[(int)type].lastCMD;
                    TimeSpan needed = type == PlayType.Single ? new TimeSpan(0, 0, 30) : new TimeSpan(0, 2, 0);
                    e.Channel.SendMessage("Oi " + e.Author.Username + " cool it will you spamming commands like a little fuck. will need to wait " + (needed - span).TotalSeconds.ToString("0.#") + "s for " + type.ToString() + " commands to be allowed");
                    return;
                }


                foreach (string clipName in plays)
                {
                    if (m_clips.ContainsKey(clipName))
                    {
                        PlayInfo info = new PlayInfo();
                        info.clip = m_clips[clipName].File;
                        if (File.Exists(info.clip))
                        {
                            if (e.Author.CurrentVoiceChannel != null)
                            {
                                info.channel = e.Author.CurrentVoiceChannel;
                            }
                            else
                            {
                                //info.channel = ActiveClient.Server.Channels.First(c => c.Type == ChannelType.Voice);
                                info.channel = ActiveClient.Server.Channels.First(c => c.Name == "General" && c.Type == ChannelType.Voice);
                            }

                            m_clips[clipName].PlayCount++;
                            UpdateClipPlay(clipName, m_clips[clipName]);

                            lock (m_mutex)
                            {
                                m_clipQueue.Enqueue(info);
                            }
                        }
                        else
                        {
                            e.Channel.SendMessage("Err: clip file could not be found maybe remove clip");
                        }
                    }
                }
            }
        }

        public void Intro(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            if (parameters.Contains("set"))
            {
                if (m_intros.ContainsKey(e.Author.ID))
                {
                    e.Channel.SendMessage(e.Author.Username + ". Come on man cant have more than 1 intro.");
                    return;
                }

                string clip = string.Empty;
                if (parameters.Contains("<clip>"))
                {
                    ValueObject param = (ValueObject)parameters["<clip>"];
                    clip = (string)param.Value;
                }

                if (!clip.EndsWith(".mp3"))
                {
                    e.Channel.SendMessage("Err: Cant add fine as its not in .mp3 format " + clip);
                    return;
                }
                else
                {
                    using (WebClient client = new WebClient())
                    {
                        string saveLocation = Path.Combine(ActiveClient.DataLocation, c_clipFolder, e.Author.ID + ".mp3");
                        string saveShortLocation = Path.Combine(ActiveClient.DataShortLocation, c_clipFolder, e.Author.ID + ".mp3");
                        if (File.Exists(saveLocation))
                        {
                            File.Delete(saveLocation);
                        }
                        Thread.Sleep(500);
                        client.DownloadFile(clip, saveLocation);
                        AddIntro(saveShortLocation, e.Author.ID);
                        m_intros.Add(e.Author.ID, saveShortLocation);
                    }
                }

                e.Channel.SendMessage("Added intro for user " + e.Author.ID);
            }
            else if (parameters.Contains("remove"))
            {
                if (m_intros.ContainsKey(e.Author.ID))
                {
                    m_intros.Remove(e.Author.ID);
                    RemoveIntro(e.Author.ID);
                }
            }
        }

        public void CheckIntro(ListDictionary parameters, DiscordVoiceStateUpdateEventArgs e)
        {
            lock (m_mutex)
            {
                if (m_currentConnected.ContainsKey(e.User.ID))
                {
                    if (m_currentConnected[e.User.ID].CurrentVoiceChannel != null)
                    {
                        return;
                    }
                    else if (m_currentConnected[e.User.ID].CurrentVoiceChannel == null)
                    {
                        m_currentConnected.Remove(e.User.ID);
                    }
                }
                else if (e.User.CurrentVoiceChannel != null)
                {
                    m_currentConnected.Add(e.User.ID, e.User);
                }
            }

            if (m_intros.ContainsKey(e.User.ID) && e.Channel != null)
            {
                PlayInfo info = new PlayInfo();
                info.clip = m_intros[e.User.ID];
                if (File.Exists(info.clip))
                {

                    if (e.User.CurrentVoiceChannel != null)
                    {
                        info.channel = e.User.CurrentVoiceChannel;
                    }
                    else
                    {
                        info.channel = e.Channel;
                    }

                    lock (m_mutex)
                    {
                        m_clipQueue.Enqueue(info);
                    }
                }
                else
                {
                    e.Channel.SendMessage("Err: intro file could not be found maybe remove intro");
                }
            }
        }

        public bool CanUserPlay(DiscordMember memeber, PlayType type)
        {
            if (m_coolDown.ContainsKey(memeber.ID))
            {
                UserCD cd = m_coolDown[memeber.ID];
                if (cd.CoolDown[(int)type].amount + 1 > 10)
                {
                    TimeSpan length = DateTime.Now - cd.CoolDown[(int)type].lastCMD;
                    if (length.Seconds >= 20)
                    {
                        cd.CoolDown[(int)type].amount = 0;
                        cd.CoolDown[(int)type].lastCMD = DateTime.Now;
                        return true;
                    }
                }
                else
                {
                    if (type == PlayType.Single)
                    {
                        cd.CoolDown[(int)type].amount++;
                        cd.CoolDown[(int)type].lastCMD = DateTime.Now;
                        return true;
                    }
                    else
                    {
                        TimeSpan length = DateTime.Now - cd.CoolDown[(int)type].lastCMD;
                        if (length.TotalMinutes >= 1)
                        {
                            cd.CoolDown[(int)type].lastCMD = DateTime.Now;
                            return true;
                        }
                    }
                }

                return false;
            }
            else
            {
                UserCD cd = new UserCD();
                cd.CoolDown[0].lastCMD = DateTime.MinValue;
                cd.CoolDown[0].amount = 0;
                cd.CoolDown[1].lastCMD = DateTime.MinValue;
                cd.CoolDown[2].lastCMD = DateTime.MinValue;

                m_coolDown.Add(memeber.ID, cd);

                return true;
            }
        }

        public override void Dispose()
        {
            if (m_consumerThread != null)
            {
                lock (m_mutex)
                {
                    m_clipQueue.Clear();
                    m_consume = false;
                }
            }
        }

        #region Playback

        public void ConsumerThread()
        {
            DiscordVoiceClient vc = null;
            bool keepConsuming = true;
            while (keepConsuming)
            {
                try
                {
                    if (m_clipQueue.Count == 0)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }

                    vc = ActiveClient.Client.GetVoiceClient(ActiveClient.Server);

                    PlayInfo info;
                    lock (m_mutex)
                    {
                        info = m_clipQueue.Dequeue();

                        Logging.LogInfo(LogType.Client, "Processing Clip " + info.clip);
                        if (m_currentChannel == null || info.channel != m_currentChannel || vc == null)
                        {
                            if (vc != null)
                                ActiveClient.Client.DisconnectFromVoice(vc);

                            ConnectToChannel(info.channel);
                            m_currentChannel = info.channel;
                            vc = ActiveClient.Client.GetVoiceClient(ActiveClient.Server);
                        }
                    }

                    if (vc == null)
                    {

                        using (DiscordDebugLogger logger = new DiscordDebugLogger())
                        {
                            Logging.LogError(LogType.Client, "Err: voice client not connected.");
                            continue;
                        }
                    }

                    try
                    {
                        int ms = vc.VoiceConfig.FrameLengthMs;
                        int channels = vc.VoiceConfig.Channels;
                        int sampleRate = 48000;

                        int blockSize = 48 * 2 * channels * ms; //sample rate * 2 * channels * milliseconds
                        byte[] buffer = new byte[blockSize];
                        var outFormat = new WaveFormat(sampleRate, 16, channels);

                        Logging.LogInfo(LogType.Client, "Starting to sample");

                        vc.SetSpeaking(true);
                        using (var mp3Reader = new MediaFoundationReader(info.clip))
                        {
                            using (var resampler = new MediaFoundationResampler(mp3Reader, outFormat) { ResamplerQuality = 20 })
                            {
                                int byteCount;
                                while ((byteCount = resampler.Read(buffer, 0, blockSize)) > 0)
                                {
                                    if (vc.Connected)
                                    {
                                        vc.SendVoice(buffer);
                                    }
                                    else
                                        break;
                                }

                                resampler.Dispose();
                                mp3Reader.Close();
                            }
                        }

                        Logging.LogInfo(LogType.Client, "Clip " + info.clip + " queue in voice channel.");

                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            Logging.LogException(LogType.Client, ex, "Failed to decode audio file");
                        }
                        catch { }
                    }

                    lock (m_mutex)
                    {
                        keepConsuming = m_consume;
                    }
                }
                catch (Exception ex)
                {
                    Logging.LogException(LogType.Client, ex, "Failed to decode audio file");
                }
            }

            vc = ActiveClient.Client.GetVoiceClient(ActiveClient.Server);
            if (vc != null)
                ActiveClient.Client.DisconnectFromVoice(vc);

            Logging.LogInfo(LogType.Client, "Exiting Audio Thread");
        }

        public bool ConnectToChannel(DiscordChannel channel)
        {
            if (channel != null)
            {
                DiscordVoiceConfig config = new DiscordVoiceConfig
                {
                    FrameLengthMs = 60,
                    Channels = 1,
                    OpusMode = DiscordSharp.Voice.OpusApplication.MusicOrMixed,
                    SendOnly = true
                };

                ActiveClient.Client.ConnectToVoiceChannel(ActiveClient.Server, channel, config);
                int tryCount = 10;
                while (ActiveClient.Client.GetVoiceClient(ActiveClient.Server) == null && tryCount-- > 0)
                {
                    System.Threading.Thread.Sleep(500);
                }

                if (tryCount == 0)
                    return false;
                else
                    return true;
            }

            else return false;
        }

        #endregion
    }

}
