﻿using DiscordDomain.Admin;
using DiscordEngine.Helpers;
using DiscordSharp.Events;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

//ASM:Newtonsoft.Json.dll
namespace DiscordDomain.CSharp
{
    public class esport : DiscordScript
    {
        public struct Reminder
        {
            public string Pattern;
            public DateTime LastSent;
        }

        public Dictionary<string, List<Reminder>> m_reminders = new Dictionary<string, List<Reminder>>();

        private const string c_dota2Api     = "http://dailydota2.com/match-api";
        private const string c_dataFolder   = "esports";
        private const string c_reminderXml  = "reminders.xml";

        private object m_mutex = new object();

        public override void RegisterCommands()
        {
            ActiveClient.RegisterCommand("esport", "(add | remove) reminder <reminder>", "Adds or Removes a reminder", ReminderEvent);
            ActiveClient.RegisterCommand("esport", "reminder list", "Lists all the remindsers set", ReminderEvent);
            ActiveClient.RegisterCommand("esport", "games list", "Lists all the upcoming games", ListGames);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public void Initalise()
        {
            string dataFolder = Path.Combine(ActiveClient.DataLocation, c_dataFolder);
            if (!Directory.Exists(dataFolder))
            {
                Directory.CreateDirectory(dataFolder);
            }
        }

        public void LoadReminders()
        {
            XDocument remindersXml;
            string reminderStore = Path.Combine(ActiveClient.DataLocation, c_dataFolder, c_reminderXml);
            if (File.Exists(reminderStore))
            {
                remindersXml = XDocument.Load(reminderStore);
                foreach (XElement userElement in remindersXml.Root.Elements())
                {
                    string user = string.Empty;
                    if (userElement.Attribute("user") != null)
                        user = userElement.Attribute("user").Value;

                    if (string.IsNullOrEmpty(user))
                        continue;

                    List<XElement> remindersToRemove = new List<XElement>();
                    foreach (XElement reminderElement in userElement.Elements())
                    {
                        string pattern = string.Empty;
                        DateTime lastSent = DateTime.MinValue;
                        if (reminderElement.Attribute("pattern") != null)
                            pattern = reminderElement.Attribute("pattern").Value;
                        if (reminderElement.Attribute("last") != null)
                        {
                            string attri = reminderElement.Attribute("last").Value;
                            lastSent = DateTime.Parse(attri);
                        }

                        if((DateTime.Now - lastSent).TotalDays > 30)
                        {
                            remindersToRemove.Add(reminderElement);
                            continue;
                        }

                        if (!string.IsNullOrEmpty(pattern))
                        {
                            try
                            {
                                Reminder rm = new Reminder();
                                rm.Pattern  = pattern;
                                rm.LastSent = lastSent;

                                lock (m_mutex)
                                {
                                    if(m_reminders.ContainsKey(user))
                                    {
                                        m_reminders[user].Add(rm);
                                    }
                                    else
                                    {
                                        m_reminders.Add(user, new List<Reminder>());
                                        m_reminders[user].Add(rm);
                                    }
                                }
                            }
                            catch
                            {
                                using (DiscordDebugLogger logger = new DiscordDebugLogger())
                                {
                                    Logging.LogError(LogType.Client, "Cant add user " + user + " as they have already been registered");
                                }
                            }
                        }
                    }

                    for (int i = 0; i < remindersToRemove.Count; i++)
                        remindersToRemove[i].Remove();
                }

                remindersXml.Save(reminderStore);
            }
            else
            {
                remindersXml = new XDocument();
                XElement root = new XElement("EsportReminders");
                remindersXml.Add(root);
                remindersXml.Save(reminderStore);
            }
        }

        public void AddReminder(string userID, string tag, DateTime lastTriggered)
        {
            XDocument reminderStoreXml;
            string clipStore = Path.Combine(ActiveClient.DataLocation, c_dataFolder, c_reminderXml);
            if (File.Exists(clipStore))
            {
                reminderStoreXml = XDocument.Load(clipStore);
                XElement user = reminderStoreXml.Root.Elements().FirstOrDefault(e => e.Attribute("user") != null && e.Attribute("user").Value == userID);
                if(user == null)
                {
                    user = new XElement("UserReminder");
                    user.SetAttributeValue("user", userID);
                    reminderStoreXml.Root.Add(user);
                }

                XElement reminder = new XElement("Reminder");
                reminder.Add(new XAttribute("pattern", tag));
                reminder.Add(new XAttribute("last", lastTriggered));
                user.Add(reminder);
                reminderStoreXml.Save(clipStore);
            }
            else
            {
                reminderStoreXml        = new XDocument();
                XElement root           = new XElement("EsportReminders");
                XElement userReminder   = new XElement("UserReminder");
                userReminder.SetAttributeValue("user", userID);

                XElement reminder       = new XElement("Reminder");
                reminder.Add(new XAttribute("pattern", tag));
                reminder.Add(new XAttribute("last", lastTriggered));
                userReminder.Add(reminder);
                root.Add(userReminder);
                reminderStoreXml.Add(root);
                reminderStoreXml.Save(clipStore);
            }
        }

        public void RemoveReminder(string userID, string tag)
        {
            /*XDocument introStoreXml;
            string introStore = Path.Combine(ActiveClient.DataLocation, c_clipFolder, c_introStore);
            if (File.Exists(introStore))
            {
                introStoreXml = XDocument.Load(introStore);
                XElement intro = new XElement("AudioIntro");
                intro.Add(new XAttribute("name", file));
                intro.Add(new XAttribute("user", userID));
                introStoreXml.Root.Add(intro);
                introStoreXml.Save(introStore);
            }
            else
            {
                introStoreXml = new XDocument();
                XElement root = new XElement("AudioIntroStore");
                XElement intro = new XElement("AudioIntro");
                intro.Add(new XAttribute("name", file));
                intro.Add(new XAttribute("user", userID));
                root.Add(intro);
                introStoreXml.Add(root);
                introStoreXml.Save(introStore);
            }*/
        }

        public void ReminderEvent(ListDictionary parameters, DiscordMessageEventArgs e)
        {
        }
        
        public void ListGames(ListDictionary parameters, DiscordMessageEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("__**Upcoming Games**__\n");
            dynamic games = GetGames();
            if (games != null)
            {
                foreach(var match in games.matches)
                {
                    int startTime = match.starttime_unix;
                    DateTime startTimeLocal = UnixTimeStampToDateTime(startTime);
                    string team1 = match.team1.team_name;
                    string team2 = match.team2.team_name;
                    string league = match.league.name;
                    int series = match.series_type;
                    string link = match.link;

                    string msg = string.Format("`{0} Best Of {1} Starts at {2} ({3})`\n**{4}** vs **{5}**\n{6}\n---------------------------------------\n",
                        league, series, startTimeLocal.ToString("HH:mm:ss d/M/yy"), (startTimeLocal - DateTime.Now).ToString(@"hh\:mm"), team1, team2, link);

                    sb.AppendLine(msg);
                }
            }

            e.Author.SendMessage(sb.ToString());
        }

        private dynamic GetGames()
        {
            string json = string.Empty;
            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString(c_dota2Api);
            }
            if (!string.IsNullOrEmpty(json))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            }

            return null;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }

}
